%define real_version %((cat %{_sourcedir}/VERSION || cat %{_builddir}/VERSION || echo UNKNOWN) 2>/dev/null)
%define top_directory /opt/mbtf

Name:		mbtf
Version:	%{real_version}
Release:	1%{?dist}
Summary:	Message Broker Testing Framework
Group:		Applications/Internet
License:	Apache-2.0
URL:		https://gitlab.cern.ch/mig/%{name}
Source0:	%{name}-%{version}.tgz
Source1:	VERSION
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch:	noarch
BuildRequires:	perl

%description
This is the Message Broker Testing Framework (MBTF). Its purpose is to ease
the testing of messaging brokers by providing high-level Perl libraries that
can be used to build extensive tests.

%prep
%setup -q -n %{name}-%{version}

%build
make build

%install
make install INSTROOT=%{buildroot} BINDIR=%{_bindir} MANDIR=%{_mandir} TOPDIR=%{top_directory}

%clean
make clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README* Changes eg
%{_bindir}/*
%{_mandir}/man?/*
%{top_directory}
