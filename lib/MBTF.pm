#+##############################################################################
#                                                                              #
# File: MBTF.pm                                                                #
#                                                                              #
# Description: Message Broker Testing Framework                                #
#                                                                              #
#-##############################################################################

# $Revision: 2296 $

package MBTF;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use Authen::Credential qw();
use Config::Validator qw(string2hash);
use MIME::Base64 qw(encode_base64);
use No::Worries::Die qw(dief);
use Params::Validate qw(:types);
use Time::HiRes qw();

#
# global variables
#

our(%Value, $RndCnt);

#
# constants
#

use constant B62 => join("", 0 .. 9, "a" .. "z", "A" .. "Z");

#+++############################################################################
#                                                                              #
# Params::Validate helpers                                                     #
#                                                                              #
#---############################################################################

#
# mark some options as being optional
#

sub spec_optional ($@) {
    my($spec, @names) = @_;

    foreach my $name (@names) {
        dief("unknown spec option: %s", $name) unless $spec->{$name};
        $spec->{$name}{"optional"} = 1;
        $spec->{$name}{"type"} |= UNDEF;
    }
}

#+++############################################################################
#                                                                              #
# option helpers                                                               #
#                                                                              #
#---############################################################################

#
# handle authentication options: default to <none>, can be given as string too
#

sub option_auth ($@) {
    my($option, @names) = @_;

    foreach my $name (@names) {
        if (defined($option->{$name})) {
            $option->{$name} = Authen::Credential->parse($option->{$name})
                unless ref($option->{$name});
            $option->{$name}->check();
        } else {
            $option->{$name} = Authen::Credential->parse("none");
        }
    }
}

#
# handle hash options: default to {}, can be given as string too
#

sub option_hash ($@) {
    my($option, @names) = @_;

    foreach my $name (@names) {
        if (defined($option->{$name})) {
            $option->{$name} = string2hash($option->{$name})
                unless ref($option->{$name});
        } else {
            $option->{$name} = {};
        }
    }
}

#
# handle numerical options: default to 0
#

sub option_numerical ($@) {
    my($option, @names) = @_;

    foreach my $name (@names) {
        $option->{$name} = 0 unless defined($option->{$name});
    }
}

#
# handle string options: default to ""
#

sub option_string ($@) {
    my($option, @names) = @_;

    foreach my $name (@names) {
        $option->{$name} = "" unless defined($option->{$name});
    }
}

#+++############################################################################
#                                                                              #
# randomization helpers                                                        #
#                                                                              #
#---############################################################################

#
# quite random/unique alphanumerical string
#

sub rndstr () {
    my(@time, $rnd, $str);

    @time = Time::HiRes::gettimeofday();
    $rnd = pack("CCCCn", $$ % 256, $time[0] % 256, $time[1] % 256,
                $RndCnt++ % 256, int(rand(65536)));
    $str = encode_base64($rnd, "");
    $str =~ s/[\/\+]/substr(B62,int(rand(62)),1)/eg;
    return($str);
}

#+++############################################################################
#                                                                              #
# variable substitution helpers                                                #
#                                                                              #
#---############################################################################

#
# variable substitution (recursively)
#

sub varsub ($);
sub varsub ($) {
    my($data) = @_;
    my($changed);

    return() unless defined($data);
    if (ref($data)) {
        ## no critic 'BuiltinFunctions::ProhibitVoidMap'
        return([ map { varsub($_) } @{ $data } ])
            if ref($data) eq "ARRAY";
        return({ map { $_ => varsub($data->{$_}) } keys(%{ $data }) })
            if ref($data) eq "HASH";
        return($data);
    }
    while ($data =~ /\<\{\S+\}\>/) {
        $changed = 0;
        foreach my $name (keys(%Value)) {
            $changed++ if $data =~ s/\<\{$name\}\>/$Value{$name}/g;
        }
        last unless $changed;
    }
    if ($data =~ /\<\{(\S+?)\}\>/) {
        dief("unknown variable (%s) in string: %s", $1, $_[0]);
    }
    return($data);
}

#+++############################################################################
#                                                                              #
# constructor helpers                                                          #
#                                                                              #
#---############################################################################

#
# kind of SUPER::new
#

# argh! see http://www.modernperlbooks.com/mt/2009/09/when-super-isnt.html

sub _parent_new ($$$) {
    my($class, $base, $option) = @_;
    my(@isa, $new);

    {
        no strict "refs";
        @isa = @{"${base}::ISA"};
    }
    dief("ooops: no \@ISA found in %s", $base) unless @isa;
    foreach my $name (@isa) {
        $new = $name->can("new");
        return($new->($class, %{ $option })) if $new;
    }
    dief("ooops: no new() found in %s", $base);
}

#
# constructor helper taking care of options
#

sub super_new ($$$$) {
    my($class, $base, $spec, $option) = @_;
    my(%extra, $object);

    # remove all the known options if they _exist_
    foreach my $name (keys(%{ $spec })) {
        next unless exists($option->{$name});
        $extra{$name} = delete($option->{$name});
    }
    # call the parent constructor with what is left
    $object = _parent_new($class, $base, $option);
    # add back all the deleted options if they are _defined_
    foreach my $name (keys(%extra)) {
        next unless defined($extra{$name});
        $object->{$name} = $extra{$name};
    }
    # so far so good
    return($object);
}

#
# module initialization
#

$Value{PREFIX} = "test.mbtf.";  # prefix used for all destination names
$Value{MAX_THREAD_TIME} = 300;  # maximum execution time for a thread
$Value{MAX_DRAIN_TIME} = 60;    # maximum drain time (used as safeguard)
$Value{MAX_RECEIPT_TIME} = 30;  # maximum time spent waiting for receipts
$Value{MAX_ACK_TIME} = 30;      # maximum time spent trying to send acks

$RndCnt = 0;

1;
