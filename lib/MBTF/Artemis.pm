#+##############################################################################
#                                                                              #
# File: MBTF/Artemis.pm                                                        #
#                                                                              #
# Description: grouping of all Artemis tests                                   #
#                                                                              #
#-##############################################################################

# $Revision: 2708 $

package MBTF::Artemis;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use MBTF::Test::Acknowledgment qw();
use MBTF::Test::BasicBlock qw();
use MBTF::Test::Count qw();
use MBTF::Test::Expiration qw();
use MBTF::Test::Independent qw();
use MBTF::Test::MessageId qw();
use MBTF::Test::Queue::Balance qw();
use MBTF::Test::Queue::Slice qw();
use MBTF::Test::Selector qw();
use MBTF::Test::Size qw();
use MBTF::Test::Transaction qw();
use MBTF::Test::Wildcard qw();
use MBTF::Test::WYPIWYC qw();
use No::Worries::Die qw(dief);
use No::Worries::Proc qw(proc_output);

#
# expected sender headers (assuming populate-validated-user=true)
#

sub sender ($) {
    my($auth) = @_;
    my($tmp);

    dief("missing authentication") unless $auth;
    return() if $auth->scheme() eq "none";
    return(JMSXUserID => "=" . $auth->name()) if $auth->scheme() eq "plain";
    if ($auth->scheme() eq "x509") {
        $tmp = proc_output(qw(openssl x509 -subject -noout -nameopt RFC2253
                              -nameopt sep_comma_plus_space -in),
                           $auth->cert());
        dief("unexpected subject: %s", $tmp)
            unless $tmp =~ s/^subject\s*=\s*//;
        if ($tmp =~ /^CN=(.+?),.+OU=Users, OU=Organic Units, DC=cern, DC=ch$/) {
            $tmp = $1;
            $tmp =~ s/\s+/./g;
            return(JMSXUserID => "=" . $tmp);
        } else {
            dief("unexpected DN: %s", $tmp);
        }
    }
    dief("unexpected authentication: %s", $auth->string());
}

#
# durable subscription creation
#

sub dsub ($$) {
    my($test, $suffix) = @_;
    my(%subscribe);

    %subscribe = %{ $test->{"consumer-subscribe"} };
    $subscribe{persistent} = "true";
    $subscribe{id} = "<{PREFIX}><{RND1}>.$suffix";
    local $test->{"consumer-subscribe"} = \%subscribe;
    local $test->{"drain"} = 1;
    MBTF::Test::BasicBlock::drain($test, {});
    return(destination => "/dsub/$subscribe{id}");
}

#
# selector expression modifier (see the "Filter Expressions" page of the User Manual)
#

sub selmod ($) {
    my($test) = @_;

    if ($test =~ /::t_field_dash$/) {
        return("hyphenated_props:");
    } elsif ($test =~ /::t_number_/) {
        return("convert_string_expressions:");
    } else {
        return("");
    }
}

#
# all tests expected to succeed
#

sub test_all (%) {
    my(%option) = @_;
    my(@sender);

    if ($option{"sender"}) {
        @sender = @{ delete($option{"sender"}) };
    } else {
        @sender = sender($option{"producer-auth"} || $option{"auth"});
    }
    foreach my $type (qw(topic queue)) {
        local $option{destination} = "/$type/<{PREFIX}><{RND0}>";
        MBTF::Test::Acknowledgment->new(%option)
            if $type eq "queue";
        MBTF::Test::BasicBlock->new(%option, "dsub" => \&dsub)
            if $type eq "topic" and 0; ###UNTESTED###
        MBTF::Test::Count->new(%option,
            "duration"       => 5,
            "producer-count" => 3,
            "consumer-count" => 3,
        );
        MBTF::Test::Expiration->new(%option);
        MBTF::Test::MessageId->new(%option);
        MBTF::Test::Queue::Balance->new(%option, duration => 5)
            if $type eq "queue";
        MBTF::Test::Queue::Slice->new(%option, duration => 5)
            if $type eq "queue";
        MBTF::Test::Selector->new(%option, modifier => \&selmod);
        MBTF::Test::Size->new(%option);
        MBTF::Test::Transaction->new(%option) if 0; ###UNTESTED### ARTEMIS-406
        MBTF::Test::Wildcard->new(%option,
            "destination"   => "/$type/<{PREFIX}><{RND0}><{SUFFIX}>",
            "name-wildcard" => "*",
            "path-wildcard" => "#",
        );
        MBTF::Test::WYPIWYC->new(%option,
            expect => {
                "destination-type" => "~(ANYCAST|MULTICAST)",
                "expires"           => "~\\d+",
                "persistent"        => "~(false|true)",
                "priority"          => "~\\d+",
                "redelivered"       => "=false",
                "timestamp"         => "~\\d+",
                @sender,
            },
        );
    }
    # topic/queue independence
    MBTF::Test::Independent->new(%option,
          "producer-destination" => "/topic/<{PREFIX}><{RND0}>",
          "consumer-destination" => "/queue/<{PREFIX}><{RND0}>",
    );
    MBTF::Test::Independent->new(%option,
          "producer-destination" => "/queue/<{PREFIX}><{RND0}>",
          "consumer-destination" => "/topic/<{PREFIX}><{RND0}>",
    );
}

1;
