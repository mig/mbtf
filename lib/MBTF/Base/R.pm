#+##############################################################################
#                                                                              #
# File: MBTF/Base/R.pm                                                         #
#                                                                              #
# Description: base class with randomization                                   #
#                                                                              #
#-##############################################################################

# $Revision: 1020 $

#
# Setups:
#  - _randomize: set the RND* global variables
#

package MBTF::Base::R;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base);

#
# setup: randomize
#

__PACKAGE__->add_testinfo("_randomize", "setup" => 0);
sub _randomize : method {
    my($self) = @_;

    foreach my $n (0 .. 9) {
        $MBTF::Value{"RND$n"} = MBTF::rndstr();
    }
}

1;
