#+##############################################################################
#                                                                              #
# File: MBTF/Base/C.pm                                                         #
#                                                                              #
# Description: base class for tests with one endpoint only                     #
#                                                                              #
#-##############################################################################

# $Revision: 1142 $

#
# Options:
#  - auth|connect|disconnect|uri: standard endpoint definition
#

package MBTF::Base::C;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base);
use Params::Validate qw(validate_with :types);

#
# constructor
#

# here is what can be given to new():

my %new_spec1 = (
    "auth"       => { type => SCALAR | OBJECT  },
    "connect"    => { type => SCALAR | HASHREF },
    "disconnect" => { type => SCALAR | HASHREF },
    "sockopts"   => { type => SCALAR | HASHREF },
    "uri"        => { type => SCALAR },
);
MBTF::spec_optional(\%new_spec1, qw(auth connect disconnect sockopts));

# here is what will appear in the object:

my %new_spec2 = (
    "auth"       => { type => OBJECT  },
    "connect"    => { type => HASHREF },
    "disconnect" => { type => HASHREF },
    "sockopts"   => { type => HASHREF },
    "uri"        => { type => SCALAR  },
);

sub new : method {
    my($class, %option, $object);

    $class = shift(@_);
    %option = validate_with(
        params      => \@_,
        spec        => \%new_spec1,
        allow_extra => 1,
    );
    $object = MBTF::super_new($class, __PACKAGE__, \%new_spec2, \%option);
    MBTF::option_auth($object, qw(auth));
    MBTF::option_hash($object, qw(connect disconnect sockopts));
    # re-check for mandatory options
    validate_with(
        params      => [ %{ $object } ],
        spec        => \%new_spec2,
        allow_extra => 1,
    );
    # so far so good
    return($object);
}

1;
