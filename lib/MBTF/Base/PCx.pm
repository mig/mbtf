#+##############################################################################
#                                                                              #
# File: MBTF/Base/PCx.pm                                                       #
#                                                                              #
# Description: base class for tests with 1 producer and 1 consumer endpoints   #
#                                                                              #
#-##############################################################################

# $Revision: 2670 $

#
# This is identical to MBTF::Base::PC but with, in addition, setup/teardown
# methods to create/destroy the STOMP connections to the endpoints
#

package MBTF::Base::PCx;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PC);
use MBTF::Support::STOMP qw();

#
# setup: create the two STOMP connections
#

__PACKAGE__->add_testinfo("setup", "setup" => 0);
sub setup : method {
    my($self) = @_;
    my(%option);

    %option = $self->producer_option();
    delete(@option{qw(disconnect destination)});
    $self->{producer} = MBTF::Support::STOMP::connect(%option);
    $self->debug("connected producer");
    %option = $self->consumer_option();
    delete(@option{qw(disconnect subscribe)});
    $self->{consumer} = MBTF::Support::STOMP::connect(%option);
    $self->debug("connected consumer");
}

#
# teardown: destroy the two STOMP connections
#

__PACKAGE__->add_testinfo("teardown", "teardown" => 0);
sub teardown : method {
    my($self) = @_;
    my(%option);

    if ($self->{producer}) {
        unless ($self->{producer_error}) {
            %option = $self->producer_option();
            MBTF::Support::STOMP::disconnect($self->{producer},
                                             %{ $option{disconnect} });
        }
        delete($self->{producer});
        $self->debug("disconnected producer");
    }
    if ($self->{consumer}) {
        unless ($self->{consumer_error}) {
            %option = $self->consumer_option();
            MBTF::Support::STOMP::disconnect($self->{consumer},
                                             %{ $option{disconnect} });
        }
        delete($self->{consumer});
        $self->debug("disconnected consumer");
    }
}

1;
