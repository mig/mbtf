#+##############################################################################
#                                                                              #
# File: MBTF/Base/PCd.pm                                                       #
#                                                                              #
# Description: base class for tests with 1 producer and 1 consumer endpoints   #
#                                                                              #
#-##############################################################################

# $Revision: 1020 $

#
# This is identical to MBTF::Base::PC but with, in addition, an extra duration
# option as well as default disconnect options to make sure receipts are used
# while disconnecting.
#
# Options:
#  - duration: the duration for the producer(s)
#

package MBTF::Base::PCd;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PC);
use Params::Validate qw(validate_with :types);

#
# here is what can be given to new()
#

my %new_spec1 = (
    duration => { type => SCALAR },
);
MBTF::spec_optional(\%new_spec1, keys(%new_spec1));

#
# here is what will appear in the object
#

my %new_spec2 = (
    duration => { type => SCALAR, regex => qr/^\d+$/ },
);

#
# constructor
#

sub new : method {
    my($class, %option, $object);

    $class = shift(@_);
    %option = validate_with(
        params      => \@_,
        spec        => \%new_spec1,
        allow_extra => 1,
    );
    # defaults
    $option{"duration"} ||= 1;
    # we need to make sure that the producers do send all their messages
    $option{"producer-disconnect"} ||= { receipt => 1 };
    # we need to make sure that the consumers do ack all their messages
    $option{"consumer-disconnect"} ||= { receipt => 1 };
    # create the object
    $object = MBTF::super_new($class, __PACKAGE__, \%new_spec2, \%option);
    # re-check for mandatory options
    validate_with(
        params      => [ %{ $object } ],
        spec        => \%new_spec2,
        allow_extra => 1,
    );
    # so far so good
    return($object);
}

1;
