#+##############################################################################
#                                                                              #
# File: MBTF/Base/PCxs.pm                                                      #
#                                                                              #
# Description: base class for tests with 1 producer and 1 consumer endpoints   #
#                                                                              #
#-##############################################################################

# $Revision: 1054 $

#
# This is identical to MBTF::Base::PCx but with, in addition, one setup
# method to subscribe the consumer
#

package MBTF::Base::PCxs;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PCx);
use MBTF::Support::STOMP qw();

#
# setup: subscribe the consumer
#

__PACKAGE__->add_testinfo("setup", "setup" => "+0");
sub setup : method {
    my($self) = @_;

    $self->SUPER::setup();
    $self->{"subscribe-id"} = MBTF::Support::STOMP::subscribe(
        $self->{consumer}, MBTF::varsub($self->{"consumer-subscribe"}));
}

1;
