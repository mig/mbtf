#+##############################################################################
#                                                                              #
# File: MBTF/Base/PC.pm                                                        #
#                                                                              #
# Description: base class for tests with 1 producer and 1 consumer endpoints   #
#                                                                              #
#-##############################################################################

# $Revision: 2570 $

#
# Options:
#  - (producer-|consumer-)?(auth|connect|disconnect|sockopts|timeout|uri): standard endpoint
#  - (producer-|consumer-)?(destination|subscribe): standard publish/subscribe
#  - drain: timeout used when draining
#  - type: broker behavior type wrt the destination (queue|topic)
#

package MBTF::Base::PC;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::R);
use Params::Validate qw(validate_with :types);

#
# here is what can be given to new()
#

my %new_spec1 = (
    "auth"                 => { type => SCALAR | OBJECT  },
    "connect"              => { type => SCALAR | HASHREF },
    "destination"          => { type => SCALAR           },
    "disconnect"           => { type => SCALAR | HASHREF },
    "drain"                => { type => SCALAR           },
    "sockopts"             => { type => SCALAR | HASHREF },
    "subscribe"            => { type => SCALAR | HASHREF },
    "timeout"              => { type => SCALAR           },
    "type"                 => { type => SCALAR           },
    "uri"                  => { type => SCALAR           },
    "producer-auth"        => { type => SCALAR | OBJECT  },
    "producer-connect"     => { type => SCALAR | HASHREF },
    "producer-destination" => { type => SCALAR           },
    "producer-disconnect"  => { type => SCALAR | HASHREF },
    "producer-sockopts"    => { type => SCALAR | HASHREF },
    "producer-subscribe"   => { type => SCALAR | HASHREF },
    "producer-timeout"     => { type => SCALAR           },
    "producer-uri"         => { type => SCALAR           },
    "consumer-auth"        => { type => SCALAR | OBJECT  },
    "consumer-connect"     => { type => SCALAR | HASHREF },
    "consumer-destination" => { type => SCALAR           },
    "consumer-disconnect"  => { type => SCALAR | HASHREF },
    "consumer-sockopts"    => { type => SCALAR | HASHREF },
    "consumer-subscribe"   => { type => SCALAR | HASHREF },
    "consumer-timeout"     => { type => SCALAR           },
    "consumer-uri"         => { type => SCALAR           },
);
MBTF::spec_optional(\%new_spec1, keys(%new_spec1));

#
# here is what will appear in the object
#

my %new_spec2 = (
    "drain"                => { type => SCALAR, regex => qr/^(\d+\.)?\d+$/ },
    "type"                 => { type => SCALAR, regex => qr/^(queue|topic)$/ },
    "producer-auth"        => { type => OBJECT  },
    "producer-connect"     => { type => HASHREF },
    "producer-destination" => { type => SCALAR  },
    "producer-disconnect"  => { type => HASHREF },
    "producer-sockopts"    => { type => HASHREF },
    "producer-timeout"     => { type => SCALAR  },
    "producer-uri"         => { type => SCALAR  },
    "consumer-auth"        => { type => OBJECT  },
    "consumer-connect"     => { type => HASHREF },
    "consumer-disconnect"  => { type => HASHREF },
    "consumer-sockopts"    => { type => HASHREF },
    "consumer-subscribe"   => { type => HASHREF },
    "consumer-timeout"     => { type => SCALAR  },
    "consumer-uri"         => { type => SCALAR  },
);
MBTF::spec_optional(\%new_spec2, qw(producer-timeout consumer-timeout));

#
# constructor
#

sub new : method {
    my($class, %option, $object, $dest);

    $class = shift(@_);
    %option = validate_with(
        params      => \@_,
        spec        => \%new_spec1,
        allow_extra => 1,
    );
    # set the (producer-|consumer-)* options from the shortcuts
    foreach my $name (qw(auth connect disconnect destination sockopts
                         subscribe timeout uri)) {
        if ($option{$name}) {
            $option{"producer-$name"} ||= $option{$name};
            $option{"consumer-$name"} ||= $option{$name};
        }
        delete($option{$name});
    }
    # handle destination vs. subscribe
    if ($option{"producer-subscribe"}) {
        delete($option{"producer-subscribe"});
    }
    if ($option{"consumer-destination"}) {
        $option{"consumer-subscribe"} ||=
            { destination => $option{"consumer-destination"} };
        delete($option{"consumer-destination"});
    }
    # create the object
    $object = MBTF::super_new($class, __PACKAGE__, \%new_spec2, \%option);
    MBTF::option_auth($object, qw(producer-auth consumer-auth));
    MBTF::option_hash($object, qw(producer-connect consumer-connect));
    MBTF::option_hash($object, qw(producer-disconnect consumer-disconnect));
    MBTF::option_hash($object, qw(producer-sockopts consumer-sockopts));
    MBTF::option_hash($object, qw(consumer-subscribe));
    delete($object->{"consumer-subscribe"})
        unless keys(%{ $object->{"consumer-subscribe"} });
    # handle defaults
    $object->{drain} ||= 3;
    unless ($object->{type}) {
        # educated guess...
        $dest = $object->{"producer-destination"};
        if ($dest) {
            if ($dest =~ /^\/(queue|topic)\//) {
                $object->{type} = $1;
            } elsif ($dest =~ /^jms\.(queue|topic)\./) {
                $object->{type} = $1;
            }
        }
    }
    # re-check for mandatory options
    validate_with(
        params      => [ %{ $object } ],
        spec        => \%new_spec2,
        allow_extra => 1,
    );
    # so far so good
    return($object);
}

#
# helper methods to extract the relevant options
#

sub producer_option : method {
    my($test) = @_;

    return(
        auth        => $test->{"producer-auth"},
        connect     => $test->{"producer-connect"},
        debug       => $test->{"stomp-debug"},
        disconnect  => $test->{"producer-disconnect"},
        destination => MBTF::varsub($test->{"producer-destination"}),
        sockopts    => $test->{"producer-sockopts"},
        timeout     => $test->{"producer-timeout"},
        uri         => $test->{"producer-uri"},
    );
}

sub consumer_option : method {
    my($test) = @_;

    return(
        auth        => $test->{"consumer-auth"},
        connect     => $test->{"consumer-connect"},
        debug       => $test->{"stomp-debug"},
        disconnect  => $test->{"consumer-disconnect"},
        sockopts    => $test->{"consumer-sockopts"},
        subscribe   => MBTF::varsub($test->{"consumer-subscribe"}),
        timeout     => $test->{"consumer-timeout"},
        uri         => $test->{"consumer-uri"},
    );
}

1;
