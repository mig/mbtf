#+##############################################################################
#                                                                              #
# File: MBTF/Base/Cx.pm                                                        #
#                                                                              #
# Description: base class for tests with one endpoint only                     #
#                                                                              #
#-##############################################################################

# $Revision: 1142 $

#
# This is identical to MBTF::Base::C but with, in addition, setup/teardown
# methods to create/destroy the STOMP connection to the endpoint
#

package MBTF::Base::Cx;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::C);
use MBTF::Support::STOMP qw();

#
# setup: create the STOMP connection
#

__PACKAGE__->add_testinfo("setup", "setup" => 0);
sub setup : method {
    my($self) = @_;

    $self->{"client"} = MBTF::Support::STOMP::connect(
        auth     => $self->{"auth"},
        connect  => $self->{"connect"},
        debug    => $self->{"stomp-debug"},
        sockopts => $self->{"sockopts"},
        uri      => $self->{"uri"},
    );
    $self->debug("connected");
}

#
# teardown: destroy the STOMP connection
#

__PACKAGE__->add_testinfo("teardown", "teardown" => 0);
sub teardown : method {
    my($self) = @_;

    if ($self->{"client"}) {
        MBTF::Support::STOMP::disconnect($self->{"client"},
                                         %{ $self->{"disconnect"} });
        delete($self->{"client"});
        $self->debug("disconnected");
    }
}

1;
