#+##############################################################################
#                                                                              #
# File: MBTF/Support/Client.pm                                                 #
#                                                                              #
# Description: client support for MBTF                                         #
#                                                                              #
#-##############################################################################

# $Revision: 1054 $

package MBTF::Support::Client;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use List::Util qw(sum);
use MBTF qw();
use MBTF::Support::STOMP qw();
use MBTF::Support::Thread qw();
use Test::More;

#+++############################################################################
#                                                                              #
# miscellaneous helpers                                                        #
#                                                                              #
#---############################################################################

sub sum_produced ($) {
    my($count) = @_;

    return(sum(map($count->{$_}, grep(/^prod\d*$/, keys(%{ $count })))));
}

sub sum_consumed ($) {
    my($count) = @_;

    return(sum(map($count->{$_}, grep(/^cons\d*$/, keys(%{ $count })))));
}

sub sum_drained ($) {
    my($count) = @_;

    return(sum(map($count->{$_}, grep(/^drain\d*$/, keys(%{ $count })))));
}

#+++############################################################################
#                                                                              #
# sequential clients                                                           #
#                                                                              #
#---############################################################################

#
# run simple producers sequentially
#

sub run_producers ($%) {
    my($test, %option) = @_;
    my($result, $clients, $prefix, $name);

    local $SIG{ALRM} = sub { die("producer timed out!\n") };
    $result  = delete($option{result})  || {};
    $clients = delete($option{clients}) || 1;
    $prefix  = delete($option{prefix})  || "prod";
    foreach my $i (1 .. $clients) {
        $name = $clients > 1 ? $prefix . $i : $prefix;
        alarm($MBTF::Value{MAX_THREAD_TIME});
        $result->{$name} = MBTF::Support::STOMP::produce(%option);
        alarm(0);
        $test->debug("%s produced %d", $name, $result->{$name});
    }
}

#
# run simple consumers sequentially
#

sub run_consumers ($%) {
    my($test, %option) = @_;
    my($result, $clients, $prefix, $messages, $div, $rem, $name);

    local $SIG{ALRM} = sub { die("consumer timed out!\n") };
    $result  = delete($option{result})  || {};
    $clients = delete($option{clients}) || 1;
    $prefix  = delete($option{prefix})  || "cons";
    $messages = delete($option{messages});
    if (defined($messages)) {
        # total number of messages to consume given, split it amongst clients
        $div = int($messages / $clients);
        $rem = $messages - $div * $clients;
        $option{count} = $div;
    }
    $option{subscribe} = { %{ $option{subscribe} } }; # deep copy
    $option{subscribe}{ack} = "client"; # needed for reliable counting
    foreach my $i (1 .. $clients) {
        $name = $clients > 1 ? $prefix . $i : $prefix;
        $option{count} += $rem if $rem and $i == $clients;
        alarm($MBTF::Value{MAX_THREAD_TIME});
        $result->{$name} = MBTF::Support::STOMP::consume(%option);
        alarm(0);
        $test->debug("%s consumed %d", $name, $result->{$name});
    }
}

#
# run simple drainers sequentially
#

sub run_drainers ($%) {
    my($test, %option) = @_;
    my($result, $clients, $prefix, $name);

    local $SIG{ALRM} = sub { die("drainer timed out!\n") };
    $result  = delete($option{result})  || {};
    $clients = delete($option{clients}) || 1;
    $prefix  = delete($option{prefix})  || "drain";
    foreach my $i (1 .. $clients) {
        $name = $clients > 1 ? $prefix . $i : $prefix;
        alarm($MBTF::Value{MAX_THREAD_TIME});
        $result->{$name} = MBTF::Support::STOMP::consume(%option);
        alarm(0);
        $test->debug("%s drained %d", $name, $result->{$name});
    }
}

#+++############################################################################
#                                                                              #
# parallel clients                                                             #
#                                                                              #
#---############################################################################

#
# start simple producers in parallel
#

sub start_producers ($%) {
    my($test, %option) = @_;
    my($clients, $prefix, $name);

    $clients = delete($option{clients}) || 1;
    $prefix  = delete($option{prefix})  || "prod";
    foreach my $i (1 .. $clients) {
        $name = $clients > 1 ? $prefix . $i : $prefix;
        MBTF::Support::Thread::create($name, \&MBTF::Support::STOMP::produce,
                                      %option);
    }
}

#
# start simple consumers in parallel
#

sub start_consumers ($%) {
    my($test, %option) = @_;
    my($clients, $prefix, $messages, $div, $rem, $name);

    $clients  = delete($option{clients}) || 1;
    $prefix   = delete($option{prefix})  || "cons";
    $messages = delete($option{messages});
    if (defined($messages)) {
        # total number of messages to consume given, split it amongst clients
        $div = int($messages / $clients);
        $rem = $messages - $div * $clients;
        $option{count} = $div;
    }
    $option{subscribe} = { %{ $option{subscribe} } }; # deep copy
    $option{subscribe}{ack} = "client"; # needed for reliable counting
    foreach my $i (1 .. $clients) {
        $name = $clients > 1 ? $prefix . $i : $prefix;
        $option{count} += $rem if $rem and $i == $clients;
        MBTF::Support::Thread::create($name, \&MBTF::Support::STOMP::consume,
                                      %option);
    }
}

#
# start simple drainers in parallel
#

sub start_drainers ($%) {
    my($test, %option) = @_;
    my($clients, $prefix, $name);

    $clients = delete($option{clients}) || 1;
    $prefix  = delete($option{prefix})  || "drain";
    foreach my $i (1 .. $clients) {
        $name = $clients > 1 ? $prefix . $i : $prefix;
        MBTF::Support::Thread::create($name, \&MBTF::Support::STOMP::consume,
                                      %option);
    }
}

#
# wait for all threads to finish and collect results
#

sub wait_all ($%) {
    my($test, %option) = @_;
    my($result, %status, $action);

    $result = delete($option{result}) || {};
    MBTF::Support::Thread::wait4all();
    %status = MBTF::Support::Thread::results();
    foreach my $name (sort(keys(%status))) {
        if ($status{$name}{error}) {
            fail("thread $name failed: $status{$name}{error}");
            return(0);
        }
        unless (defined($status{$name}{count})) {
            fail("thread $name failed: count not returned");
            return(0);
        }
        $result->{$name} = $status{$name}{count};
        $action = $name =~ /^prod/  ? "produced"
                : $name =~ /^cons/  ? "consumed"
                : $name =~ /^drain/ ? "drained"
                : "exchanged";
        $test->debug("%s %s %d", $name, $action, $result->{$name});
    }
    return(1);
}

1;
