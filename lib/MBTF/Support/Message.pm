#+##############################################################################
#                                                                              #
# File: MBTF/Support/Message.pm                                                #
#                                                                              #
# Description: message support for MBTF                                        #
#                                                                              #
#-##############################################################################

# $Revision: 1047 $

package MBTF::Support::Message;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use Messaging::Message::Generator qw();
use No::Worries::Die qw(dief);

#
# global variables
#

our(
    $Generator,   # current message generator
    $Randomness,  # current message generator randomness
    @Cache,       # recently generated messages
);

#
# select the message generator to use
#

sub generator ($) {
    my($rnd) = @_;
    my(%option);

    if ($rnd == 0) {
        %option = ();
    } elsif ($rnd == 1) {
        %option = (
             "text"         => "0-1",
             "body-length"  => "0-1024",
             "body-entropy" => "1-2",
        );
    } elsif ($rnd == 2) {
        %option = (
             "text"                 => "0-1",
             "body-length"          => "0-1024",
             "body-entropy"         => "1-3",
             "header-count"         => "2^6",
             "header-name-length"   => "10-20",
             "header-name-entropy"  => "1",
             "header-name-prefix"   => "rnd-",
             "header-value-length"  => "20-40",
             "header-value-entropy" => "0-3",
        );
    } else {
        dief("unsupported randomness: %s", $rnd);
    }
    $Generator = Messaging::Message::Generator->new(%option);
    $Randomness = $rnd;
}

#
# generate a new message (without caching)
#

sub generate () {
    my($message, $rnd);

    $message = $Generator->message();
    if ($Randomness == 2) {
        $rnd = int(rand(3));
        $message->header_field("persistent", $rnd == 1 ? "true" : "false")
            if $rnd;
    }
    return($message);
}

#
# fill the cache
#

sub cache ($) {
    my($count) = @_;

    @Cache = map(generate(), 1 .. $count);
}

#
# return a random message (with caching)
#

sub random () {
    return(generate()) unless @Cache;
    return($Cache[int(rand(@Cache))]->copy());
}

#
# module initialization
#

generator(0);

1;
