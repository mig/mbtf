#+##############################################################################
#                                                                              #
# File: MBTF/Support/STOMP.pm                                                  #
#                                                                              #
# Description: STOMP support for MBTF                                          #
#                                                                              #
#-##############################################################################

# $Revision: 1453 $

package MBTF::Support::STOMP;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use threads;
use Authen::Credential qw();
use MBTF qw();
use MBTF::Support::Message qw();
use Net::STOMP::Client qw();
use No::Worries::Die qw(dief);
use Params::Validate qw(validate :types);
use Thread::Semaphore qw();
use Time::HiRes qw();

#
# global variables
#

our(
    $Subscribed,   # semaphore indicating when a consumer has subscribed
    $Started,      # semaphore indicating when a consumer can start
);

#
# connect to a STOMP broker (i.e. Net::STOMP::Client's new() + connect())
#
# (the handleref option can be used so that the handler does not get DESTROY'ed
#  on failure, this way preserving the error stored in $@)
#

my %connect_common_spec = (
    "auth"      => { type => SCALAR | OBJECT },
    "connect"   => { type => HASHREF         },
    "debug"     => { type => SCALAR          },
    "sockopts"  => { type => HASHREF         },
    "timeout"   => { type => SCALAR          },
    "uri"       => { type => SCALAR          },
);
MBTF::spec_optional(\%connect_common_spec, qw(auth connect sockopts timeout));

my %connect_spec = ( %connect_common_spec,
    "handleref" => { optional => 1, type => SCALARREF },
);
MBTF::spec_optional(\%connect_spec, qw(handleref));

sub connect (@) {  ## no critic 'ProhibitBuiltinHomonyms'
    my(%option, %newopt, %conopt, $stomp);

    # option checking
    %option = validate(@_, \%connect_spec);
    # options for new()
    %newopt = ();
    $newopt{"auth"} = $option{"auth"} if $option{"auth"};
    $newopt{"debug"} = $option{"debug"} if $option{"debug"};
    $newopt{"sockopts"} = $option{"sockopts"} if $option{"sockopts"};
    $newopt{"timeout"} = $option{"timeout"} if $option{"timeout"};
    $newopt{"uri"} = $option{"uri"};
    # options for connect()
    %conopt = $option{"connect"} ? %{ $option{"connect"} } : ();
    # just do it ;-)
    $stomp = Net::STOMP::Client->new(%newopt);
    if ($conopt{"version"}) {
        if ($conopt{"version"} =~ /^1\.\d$/) {
            # valid
            $stomp->accept_version(delete($conopt{"version"}));
        } else {
            # invalid (would be refused by Net::STOMP::Client) so we hack!
            $stomp->{"accept_version"} = [ delete($conopt{"version"}) ];
        }
    }
    ${ $option{"handleref"} } = $stomp if $option{"handleref"};
    $stomp->connect(%conopt);
    # so far so good
    return($stomp);
}

#
# disconnect from a STOMP broker
#

my %disconnect_spec = (
    "receipt" => { type => SCALAR },
);
MBTF::spec_optional(\%disconnect_spec, qw(receipt));

sub disconnect ($@) {
    my($stomp, %option);

    $stomp = shift(@_);
    %option = validate(@_, \%disconnect_spec) if @_;
    if ($option{"receipt"}) {
        # with receipt (Net::STOMP::Client will wait for the RECEIPT frame)
        $stomp->disconnect(receipt => $stomp->uuid());
    } else {
        # without receipt (we wait for frames to detect a pending ERROR frame)
        $stomp->wait_for_frames(timeout => 0.1);
        $stomp->disconnect();
    }
}

#
# synchronously subscribe with a random id and a random receipt
#

my %subscribe_spec = (
    "ack"         => { type => SCALAR },
    "destination" => { type => SCALAR },
    "id"          => { type => SCALAR },
    "persistent"  => { type => SCALAR },
    "selector"    => { type => SCALAR },
);
MBTF::spec_optional(\%subscribe_spec, qw(ack id persistent selector));

sub subscribe ($@) {
    my($stomp, %option);

    $stomp = shift(@_);
    %option = validate(@_, \%subscribe_spec);
    $stomp->message_callback(sub { return(1) })
        unless $stomp->message_callback();
    $option{"id"} ||= $stomp->uuid();
    synchronous($stomp, "subscribe", %option);
    # return the id as it will be needed to unsubscribe
    return($option{"id"});
}

#
# drain a destination and return the received messages
#

sub drain ($$) {
    my($stomp, $inactivity) = @_;
    my($maxtime, $frame, @messages);

    $maxtime = Time::HiRes::time() + $inactivity;
    while (1) {
        $frame = $stomp->wait_for_frames(timeout => 0.1);
        if ($frame) {
            $maxtime = Time::HiRes::time() + $inactivity;
        } else {
            next unless Time::HiRes::time() > $maxtime;
            last;
        }
        dief("unexpected frame received: %s", $frame->command())
            unless $frame->command() eq "MESSAGE";
        push(@messages, $frame->messagify());
    }
    return(@messages);
}

#
# produce messages
#

my %produce_spec = ( %connect_common_spec,
    "callback"    => { type => CODEREF },
    "count"       => { type => SCALAR, regex => qr/^(\d+)?$/ },
    "destination" => { type => SCALAR },
    "disconnect"  => { type => HASHREF },
    "duration"    => { type => SCALAR, regex => qr/^((\d+\.)?\d+)?$/ },
);
MBTF::spec_optional(\%produce_spec, qw(callback count disconnect duration));

sub produce (@) {
    my(%option, %disopt, $stomp, $msg, $count, $maxtime);

    # option checking
    %option = validate(@_, \%produce_spec);
    dief("missing mandatory option: count or duration")
        unless $option{"count"} or $option{"duration"};
    # connection
    $stomp = MBTF::Support::STOMP::connect(
        "auth"     => $option{"auth"},
        "connect"  => $option{"connect"},
        "debug"    => $option{"debug"},
        "sockopts" => $option{"sockopts"},
        "timeout"  => $option{"timeout"},
        "uri"      => $option{"uri"},
    );
    # production
    $count = 0;
    $maxtime = Time::HiRes::time() + $option{"duration"} if $option{"duration"};
    while (1) {
        $msg = MBTF::Support::Message::random();
        $msg->header_field("destination", $option{"destination"});
        $option{"callback"}->($msg) if $option{"callback"};
        $stomp->send_message($msg);
        $count++;
        last if $maxtime and Time::HiRes::time() >= $maxtime;
        last if $option{"count"} and $count >= $option{"count"};
    }
    # disconnection
    %disopt = $option{"disconnect"} ? %{ $option{"disconnect"} } : ();
    MBTF::Support::STOMP::disconnect($stomp, %disopt);
    # so far so good
    return($count) unless wantarray();
    return(count => $count);
}

#
# consume messages (internal loop)
#

sub _consume_loop ($$) {  ## no critic 'ProhibitExcessComplexity'
    my($stomp, $option) = @_;
    my($count, $pending, $now, $maxtime, $draintime, $ack, $frame);

    $count = $pending = 0;
    if ($option->{"duration"} or $option->{"drain"}) {
        $now = Time::HiRes::time();
        $maxtime = $now + $option->{"duration"} if $option->{"duration"};
        $draintime = $now + $option->{"drain"} if $option->{"drain"};
    }
    $ack = $option->{"subscribe"}{ack};
    $ack = undef if $ack and $ack eq "auto";
    while (1) {
        $frame = $stomp->wait_for_frames(timeout => 0.1);
        if ($frame) {
            dief("unexpected frame received: %s", $frame->command())
                unless $frame->command() eq "MESSAGE";
            $count++;
            if ($ack) {
                _ack($stomp, $frame);
                $pending++;
            }
            $draintime = Time::HiRes::time() + $option->{"drain"}
                if $option->{"drain"};
            $option->{"callback"}->($frame->messagify())
                if $option->{"callback"};
        }
        if (not $frame or $pending >= 10) {
            # we try to send the pending acks from time to time...
            $stomp->send_data(timeout => 0);
            $pending = 0;
        }
        last if $option->{"count"} and $count >= $option->{"count"};
        if ($maxtime or $draintime) {
            $now = Time::HiRes::time();
            last if $maxtime and $now >= $maxtime;
            last if $draintime and $now >= $draintime;
        }
    }
    return($count);
}

#
# consume messages
#

my %consume_spec = ( %connect_common_spec,
    "callback"   => { type => CODEREF },
    "count"      => { type => SCALAR, regex => qr/^(\d+)?$/ },
    "disconnect" => { type => HASHREF },
    "drain"      => { type => SCALAR, regex => qr/^((\d+\.)?\d+)?$/ },
    "duration"   => { type => SCALAR, regex => qr/^((\d+\.)?\d+)?$/ },
    "semaphore"  => { type => SCALAR  },
    "subscribe"  => { type => HASHREF },
);
MBTF::spec_optional(\%consume_spec,
                    qw(callback count disconnect drain duration semaphore));

sub consume (@) {
    my(%option, %disopt, $stomp, $id, $count, $limit, $pending);

    # option checking
    %option = validate(@_, \%consume_spec);
    dief("missing mandatory option: count or drain or duration")
        unless $option{"count"} or $option{"drain"} or $option{"duration"};
    $option{"drain"} = $MBTF::Value{MAX_DRAIN_TIME}
        unless $option{"drain"} or $option{"duration"};
    # connection
    $stomp = MBTF::Support::STOMP::connect(
        "auth"     => $option{"auth"},
        "connect"  => $option{"connect"},
        "debug"    => $option{"debug"},
        "sockopts" => $option{"sockopts"},
        "timeout"  => $option{"timeout"},
        "uri"      => $option{"uri"},
    );
    # subscription
    $id = subscribe($stomp, $option{"subscribe"});
    if ($option{"semaphore"}) {
        # optional synchronization now that we have subscribed
        $Subscribed->up();
        $Started->down();
    }
    # consumption
    $count = _consume_loop($stomp, \%option);
    # now that we are done, unsubscribe so that the broker stops sending us
    # messages that we are not interested in (and will not ack anyway)
    $stomp->queue_frame(
        Net::STOMP::Client::Frame->new(
            command => "UNSUBSCRIBE",
            headers => { id => $id },
        )
    );
    # send any pending data (queued ACKs and UNSUBSCRIBE)
    $limit = time() + $MBTF::Value{MAX_ACK_TIME};
    $pending = $stomp->outgoing_buffer_length();
    while ($pending > 0 and time() <= $limit) {
        $stomp->receive_data(timeout => 0);
        $stomp->send_data(timeout => 0.1);
        $pending = $stomp->outgoing_buffer_length();
    }
    dief("failed to flush outgoing buffer: %d", $pending) if $pending;
    # disconnection
    %disopt = $option{"disconnect"} ? %{ $option{"disconnect"} } : ();
    MBTF::Support::STOMP::disconnect($stomp, %disopt);
    # so far so good
    return($count) unless wantarray();
    return(count => $count);
}

#
# acknowledge a message frame in a non-blocking way, using asynchronous I/O
#

sub _ack ($$) {
    my($stomp, $frame) = @_;
    my(%option, $value);

    # extract the relevant options
    $value = $frame->header("message-id");
    $option{"message-id"} = $value if defined($value);
    $value = $frame->header("subscription");
    $option{"subscription"} = $value if defined($value);
    $value = $frame->header("ack");
    $option{"id"} = $value if defined($value);
    # build the frame
    $frame = Net::STOMP::Client::Frame->new(
        "command" => "ACK",
        "headers" => \%option,
    );
    # queue it
    $stomp->queue_frame($frame);
}

#
# wrap a STOMP command to make it synchronous
#

sub synchronous ($$%) {
    my($stomp, $command, %option) = @_;

    $option{"receipt"} = $stomp->uuid();
    $stomp->$command(%option);
    $stomp->wait_for_receipts(timeout => $MBTF::Value{MAX_RECEIPT_TIME});
    dief("synchronous %s failed: receipt not received", $command)
        if $stomp->receipts();
}

#
# module initialization
#

$Subscribed = Thread::Semaphore->new(0);
$Started = Thread::Semaphore->new(0);

1;
