#+##############################################################################
#                                                                              #
# File: MBTF/Support/Thread.pm                                                 #
#                                                                              #
# Description: thread support for MBTF                                         #
#                                                                              #
#-##############################################################################

# $Revision: 1049 $

package MBTF::Support::Thread;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use threads qw();
use MBTF qw();
use No::Worries::Die qw(dief);
use No::Worries::Log qw(log_debug);
use Time::HiRes qw();

#
# global variables
#

our(
    %Name,    # tid -> name
    %Result,  # name -> result (hash)
    %Time,    # name -> start time
);

#
# create a named thread (in list context)
#

sub create ($$@) {
    my($name, $code, @args) = @_;
    my($thread, $tid);

    dief("cannot create thread %s: already exists", $name)
        if grep($_ eq $name, values(%Name));
    local $SIG{KILL} = sub { threads->exit() };
    $thread = threads->create({
        context    => "list",
        stack_size => 64*1024,
    }, $code, @args);
    $tid = $thread->tid();
    $Name{$tid} = $name;
    $Result{$name} = undef;
    $Time{$name} = time();
    log_debug("created thread %s (tid %d)", $name, $tid, { what => "thread" });
}

#
# wait for the given named threads to finish
#

sub wait4 (@) {
    my(@names) = @_;
    my($tid, $tname, %result, $error, $maxtime, %todo);

    foreach my $name (@names) {
        dief("cannot wait for thread %s: does not exist", $name)
            unless grep($_ eq $name, values(%Name));
        $todo{$name}++;
    }
    while (1) {
        foreach my $thread (threads->list()) {
            $tid = $thread->tid();
            $tname = $Name{$tid};
            dief("unexpected thread (tid %d)", $tid) unless $tname;
            if ($thread->is_joinable()) {
                %result = $thread->join();
                $error = $thread->error();
                if ($error) {
                    log_debug("joined thread %s (tid %d) with error",
                              $tname, $tid, { what => "thread" });
                    $error =~ s/\s+$//;
                    $Result{$tname} = { error => $error };
                } else {
                    log_debug("joined thread %s (tid %d) without error",
                              $tname, $tid, { what => "thread" });
                    $Result{$tname} = { %result };
                }
            } else {
                $maxtime = $Time{$tname} + $MBTF::Value{MAX_THREAD_TIME};
                next unless time() > $maxtime;
                $thread->kill("KILL")->detach();
                log_debug("killed thread %s (tid %d) because of timeout",
                          $tname, $tid, { what => "thread" });
                $Result{$tname} = { error => "timeout" };
            }
            delete($Name{$tid});
            delete($Time{$tname});
            delete($todo{$tname});
        }
        last unless keys(%todo);
        Time::HiRes::sleep(0.1);
    }
}

#
# wait for all threads to finish
#

sub wait4all () {
    wait4(values(%Name));
}

#
# retrieve the result of the given named thread
#

sub result ($) {
    my($name) = @_;

    dief("unknown thread: %s", $name) unless exists($Result{$name});
    dief("unfinished thread: %s", $name) unless $Result{$name};
    return(delete($Result{$name}));
}

#
# retrieve all the available results
#

sub results () {
    my(%available);

    foreach my $name (keys(%Result)) {
        $available{$name} = delete($Result{$name}) if $Result{$name};
    }
    return(%available);
}

1;
