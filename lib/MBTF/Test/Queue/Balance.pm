#+##############################################################################
#                                                                              #
# File: MBTF/Test/Queue/Balance.pm                                             #
#                                                                              #
# Description: test class for queue load balancing tests                       #
#                                                                              #
#-##############################################################################

# $Revision: 1020 $

package MBTF::Test::Queue::Balance;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PCd);
use List::Util qw(sum);
use MBTF::Support::Client qw();
use MBTF::Support::STOMP qw();
use Test::More;

#+##############################################################################
#                                                                              #
# helpers                                                                      #
#                                                                              #
#-##############################################################################

sub square ($) {
    my($x) = @_;

    return($x * $x);
}

sub stdev (@) {
    my(@x) = @_;
    my($count, $mean);

    $count = @x;
    $mean = sum(@x) / $count;
    return(sqrt(sum(map(square($_ - $mean), @x)) / $count));
}

#+##############################################################################
#                                                                              #
# tests                                                                        #
#                                                                              #
#-##############################################################################

#
# test sending and receiving messages in a queue
#

sub define_test ($) {
    my($n) = @_;
    my($test);

    $test = sprintf("t_%02d", $n);
    __PACKAGE__->add_testinfo($test, test => 3);
    no strict "refs";
    *{__PACKAGE__ . "::${test}"} = sub {
        my($self) = @_;
        my(%count, $mean, $sigma, $info);

        # multiple parallel drainers
        MBTF::Support::Client::start_drainers($self, $self->consumer_option(),
            drain     => $self->{drain},
            semaphore => 1,
            clients   => $n,
        );
        $MBTF::Support::STOMP::Subscribed->down($n);
        $MBTF::Support::STOMP::Started->up($n);
        # single producer
        MBTF::Support::Client::run_producers($self, $self->producer_option(),
            duration => $self->{duration},
            result   => \%count,
        );
        # wait for termination and check
        MBTF::Support::Client::wait_all($self, result => \%count) or return;
        $count{produced} = MBTF::Support::Client::sum_produced(\%count);
        $count{drained} = MBTF::Support::Client::sum_drained(\%count);
        cmp_ok($count{produced}, ">", 0, "sum(produced) > 0");
        is($count{drained}, $count{produced}, "sum(drained) == sum(produced)");
        # check standard deviation
        $mean = $count{produced} / $n;
        $sigma = stdev(map($count{$_}, grep(/^drain\d*$/, keys(%count))));
        $info = sprintf("mean=%.1f, sigma=%.1f", $mean, $sigma);
        if ($sigma / $mean < 0.01) {
            pass("good load balancing ($info)");
        } elsif ($sigma / $mean < 0.1) {
            pass("acceptable balancing ($info)");
        } else {
            fail("poor load balancing ($info)");
        }
    };
}

#
# define tests for different numbers of consumers
#

foreach my $n (2, 3, 4, 5, 7) {
    define_test($n);
}

1;
