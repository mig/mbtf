#+##############################################################################
#                                                                              #
# File: MBTF/Test/Queue/Slice.pm                                               #
#                                                                              #
# Description: test class for queue slicing tests                              #
#                                                                              #
#-##############################################################################

# $Revision: 1020 $

package MBTF::Test::Queue::Slice;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PCd);
use MBTF::Support::Client qw();
use Test::More;

#+##############################################################################
#                                                                              #
# tests                                                                        #
#                                                                              #
#-##############################################################################

#
# test sending and receiving messages in a queue
#

sub define_test ($) {
    my($n) = @_;
    my($test);

    $test = sprintf("t_%02d", $n);
    __PACKAGE__->add_testinfo($test, test => 3);
    no strict "refs";
    *{__PACKAGE__ . "::${test}"} = sub {
        my($self) = @_;
        my(%count);

        # single producer
        MBTF::Support::Client::run_producers($self, $self->producer_option(),
            duration => $self->{duration},
            result   => \%count,
        );
        # multiple sequential consumers
        MBTF::Support::Client::run_consumers($self, $self->consumer_option(),
            messages => MBTF::Support::Client::sum_produced(\%count),
            clients  => $n,
            result   => \%count,
        );
        # final drainer
        MBTF::Support::Client::run_drainers($self, $self->consumer_option(),
            drain  => $self->{drain},
            result => \%count,
        );
        # check
        $count{produced} = MBTF::Support::Client::sum_produced(\%count);
        $count{consumed} = MBTF::Support::Client::sum_consumed(\%count);
        cmp_ok($count{produced}, ">", 0, "sum(produced) > 0");
        is($count{consumed}, $count{produced},
           "sum(consumed) == sum(produced)");
        is($count{drain}, 0, "drained == 0");
    };
}

#
# define tests for different numbers of consumers
#

foreach my $n (2, 3, 7, 13, 23) {
    define_test($n);
}

1;
