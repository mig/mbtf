#+##############################################################################
#                                                                              #
# File: MBTF/Test/Wildcard.pm                                                  #
#                                                                              #
# Description: test class for wildcard subscriptions                           #
#                                                                              #
#-##############################################################################

# $Revision: 1020 $

#
# Options:
#  - name-wildcard: wilcard matching a single name in a dot-separated path (optional)
#  - path-wildcard: wilcard matching any path (optional)
#

package MBTF::Test::Wildcard;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PCd);
use List::Util qw(sum);
use MBTF::Support::Client qw();
use MBTF::Support::STOMP qw();
use Params::Validate qw(validate_with :types);
use Test::More;

#
# suffixes to use
#

use constant EXISTING_SUFFIXES => qw(
    .<{RND1}>
    .<{RND3}>
    .<{RND3}>.<{RND5}>
    .<{RND4}>.<{RND5}>
);

use constant PRODUCER_SUFFIXES => qw(
    .<{RND1}>
    .<{RND2}>
    .<{RND3}>.<{RND5}> .<{RND3}>.<{RND6}>
    .<{RND4}>.<{RND5}> .<{RND4}>.<{RND6}>
);

#+##############################################################################
#                                                                              #
# constructor                                                                  #
#                                                                              #
#-##############################################################################

# here is what can be given to new():

my %new_spec1 = (
    "name-wildcard" => { optional => 1, type => SCALAR | UNDEF },
    "path-wildcard" => { optional => 1, type => SCALAR | UNDEF },
);

# here is what will appear in the object:

my %new_spec2 = (
    "name-wildcard" => { optional => 0, type => SCALAR },
    "path-wildcard" => { optional => 0, type => SCALAR },
);

sub new : method {
    my($class, %option, $object);

    $class = shift(@_);
    %option = validate_with(
        params      => \@_,
        spec        => \%new_spec1,
        allow_extra => 1,
    );
    # defaults
    $option{"name-wildcard"} ||= "*"; # AMQP
    $option{"path-wildcard"} ||= "#"; # AMQP
    # create the object
    $object = MBTF::super_new($class, __PACKAGE__, \%new_spec2, \%option);
    # re-check for mandatory options
    validate_with(
        params      => [ %{ $object } ],
        spec        => \%new_spec2,
        allow_extra => 1,
    );
    # so far so good
    return($object);
}

#+##############################################################################
#                                                                              #
# helpers                                                                      #
#                                                                              #
#-##############################################################################

#
# send and consume one message to make sure a destination exists
#

sub precreate ($$) {
    my($test, $suffix) = @_;
    my(%count);

    local $MBTF::Value{SUFFIX} = $suffix;
    MBTF::Support::Client::start_consumers($test, $test->consumer_option(),
        count     => 1,
        semaphore => 1,
    );
    $MBTF::Support::STOMP::Subscribed->down(1);
    MBTF::Support::Client::start_producers($test, $test->producer_option(),
        count => 1,
    );
    $MBTF::Support::STOMP::Started->up(1);
    MBTF::Support::Client::wait_all($test, result => \%count) or return();
    return($count{prod} == 1 && $count{cons} == 1);
}

#
# start the wildcard consumer/drainer
#

sub start_consumer ($$) {
    my($test, $wildcard) = @_;

    local $MBTF::Value{SUFFIX} = $wildcard;
    MBTF::Support::Client::start_drainers($test, $test->consumer_option(),
        drain     => $test->{drain},
        semaphore => 1,
    );
    $MBTF::Support::STOMP::Subscribed->down(1);
}

#
# start the producers
#

sub start_producers ($@) {
    my($test, @suffixes) = @_;

    foreach my $suffix (@suffixes) {
        local $MBTF::Value{SUFFIX} = $suffix;
        MBTF::Support::Client::start_producers($test, $test->producer_option(),
            duration => $test->{duration},
            prefix   => "prod$suffix",
        );
    }
    $MBTF::Support::STOMP::Started->up(1);
}

#
# check that what has been consumed is expected
#

sub check_consumed ($$@) {
    my($test, $wildcard, @suffixes) = @_;
    my(%count);

    MBTF::Support::Client::wait_all($test, result => \%count) or return;
    $count{prod} = sum(map($count{"prod$_"}, @suffixes));
    is($count{drain}, $count{prod},
       "cons($wildcard) = " . join("+", map("prod($_)", @suffixes)));
}

#+##############################################################################
#                                                                              #
# tests                                                                        #
#                                                                              #
#-##############################################################################

#
# setup: create some destinations
#

__PACKAGE__->add_testinfo("setup", "setup" => 1);
sub setup : method {
    my($self) = @_;

    foreach my $suffix (EXISTING_SUFFIXES) {
        next if precreate($self, $suffix);
        fail("precreate($suffix)");
        return;
    }
    pass("precreate(" . join(" ", EXISTING_SUFFIXES) . ")");
}

#
# test name wildcard at first level
#

__PACKAGE__->add_testinfo("t_wildcard_name1", test => 1);
sub t_wildcard_name1 : method {
    my($self) = @_;
    my($suffix);

    $suffix = "." . $self->{"name-wildcard"};
    # start the consumer
    start_consumer($self, $suffix);
    # start the producers
    start_producers($self, PRODUCER_SUFFIXES);
    # check results
    check_consumed($self, $suffix, qw(.<{RND1}> .<{RND2}>));
}

__PACKAGE__->add_testinfo("t_wildcard_name1a", test => 1);
sub t_wildcard_name1a : method {
    my($self) = @_;
    my($suffix);

    $suffix = "." . $self->{"name-wildcard"} . ".<{RND5}>";
    # start the consumer
    start_consumer($self, $suffix);
    # start the producers
    start_producers($self, PRODUCER_SUFFIXES);
    # check results
    check_consumed($self, $suffix, qw(.<{RND3}>.<{RND5}> .<{RND4}>.<{RND5}>));
}

__PACKAGE__->add_testinfo("t_wildcard_name1b", test => 1);
sub t_wildcard_name1b : method {
    my($self) = @_;
    my($suffix);

    $suffix = "." . $self->{"name-wildcard"} . ".<{RND6}>";
    # start the consumer
    start_consumer($self, $suffix);
    # start the producers
    start_producers($self, PRODUCER_SUFFIXES);
    # check results
    check_consumed($self, $suffix, qw(.<{RND3}>.<{RND6}> .<{RND4}>.<{RND6}>));
}

#
# test name wildcard at second level
#

__PACKAGE__->add_testinfo("t_wildcard_name2a", test => 1);
sub t_wildcard_name2a : method {
    my($self) = @_;
    my($suffix);

    $suffix = ".<{RND3}>." . $self->{"name-wildcard"};
    # start the consumer
    start_consumer($self, $suffix);
    # start the producers
    start_producers($self, PRODUCER_SUFFIXES);
    # check results
    check_consumed($self, $suffix, qw(.<{RND3}>.<{RND5}> .<{RND3}>.<{RND6}>));
}

__PACKAGE__->add_testinfo("t_wildcard_name2b", test => 1);
sub t_wildcard_name2b : method {
    my($self) = @_;
    my($suffix);

    $suffix = ".<{RND4}>." . $self->{"name-wildcard"};
    # start the consumer
    start_consumer($self, $suffix);
    # start the producers
    start_producers($self, PRODUCER_SUFFIXES);
    # check results
    check_consumed($self, $suffix, qw(.<{RND4}>.<{RND5}> .<{RND4}>.<{RND6}>));
}

#
# test path wildcard
#

__PACKAGE__->add_testinfo("t_wildcard_path", test => 1);
sub t_wildcard_path : method {
    my($self) = @_;
    my($suffix);

    $suffix = "." . $self->{"path-wildcard"};
    # start the consumer
    start_consumer($self, $suffix);
    # start the producers
    start_producers($self, PRODUCER_SUFFIXES);
    # check results
    check_consumed($self, $suffix, PRODUCER_SUFFIXES);
}

1;
