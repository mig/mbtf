#+##############################################################################
#                                                                              #
# File: MBTF/Test/WYPIWYC.pm                                                   #
#                                                                              #
# Description: test class for What You Produce Is What You Consume tests       #
#                                                                              #
#-##############################################################################

# $Revision: 1054 $

#
# Options:
#  - expect: what to expect (similar's to wypiwyc's --expect option)
#

package MBTF::Test::WYPIWYC;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PCxs);
use charnames qw(:full);
use MBTF::Support::STOMP qw();
use Messaging::Message qw();
use No::Worries::Die qw(dief);
use Params::Validate qw(validate_with :types);
use Test::More;

#+##############################################################################
#                                                                              #
# constructor                                                                  #
#                                                                              #
#-##############################################################################

#
# constructor helper
#

sub check_expect ($) {
    my($option) = @_;
    my($value);

    $option->{expect} ||= {};
    # set defaults (defined in the STOMP protocol)
    $option->{expect}{"ack"} = "?~.+"
        unless exists($option->{expect}{"ack"});
    $option->{expect}{"content-length"} = "?~\\d+"
        unless exists($option->{expect}{"content-length"});
    $option->{expect}{"content-type"} = "?~.+"
        unless exists($option->{expect}{"content-type"});
    $option->{expect}{"message-id"} = "~.+"
        unless exists($option->{expect}{"message-id"});
    $option->{expect}{"subscription"} = "?~.+"
        unless exists($option->{expect}{"subscription"});
    # check what is given
    foreach my $name (keys(%{ $option->{expect} })) {
        $value = $option->{expect}{$name};
        if (defined($value)) {
            if ($value =~ /^(\??[\=\~])(.*)$/) {
                $option->{expect}{$name} = [ $1, $2 ];
                if (substr($1, -1) eq "~") {
                    eval { "foo" =~ /^($2)$/ };
                    dief("invalid expect pattern: %s",
                         $option->{expect}{$name}[1]) if $@;
                }
            } else {
                dief("invalid expect option: %s", $value);
            }
        } else {
            delete($option->{expect}{$name});
        }
    }
}

#
# constructor
#

my %new_spec = (
    expect => { optional => 1, type => HASHREF | UNDEF },
);

sub new : method {
    my($class, %option, $object);

    $class = shift(@_);
    %option = validate_with(
        params      => \@_,
        spec        => \%new_spec,
        allow_extra => 1,
    );
    check_expect(\%option);
    $object = MBTF::super_new($class, __PACKAGE__, \%new_spec, \%option);
    # now that we know what to expect, we know how many tests we will execute...
    foreach my $test ($object->_get_methods("test")) {
        $object->num_method_tests($test, 2 + keys(%{ $object->{expect} }));
    }
    return($object);
}

#+##############################################################################
#                                                                              #
# helpers                                                                      #
#                                                                              #
#-##############################################################################

#
# send one message
#

sub send_message ($$) {
    my($self, $message) = @_;
    my($stomp, $caller);

    $caller = (caller(1))[3];
    if ($caller =~ /::t_(\w+)$/) {
        $message->header_field("x-probe", $1);
    } else {
        dief("unexpected caller: %s", $caller);
    }
    $stomp = $self->{producer};
    $message->header_field(
        "destination" => MBTF::varsub($self->{"producer-destination"}),
    );
    $message->header_field("x-type" => __PACKAGE__);
    $message->header_field("x-uuid" => $stomp->uuid());
    $stomp->send_message($message);
    pass(sprintf("sent probe %s as %s",
                 $message->header_field("x-probe"),
                 $message->header_field("x-uuid"),
    ));
}

#
# check and clean a received message
#

sub compare_messages ($$$) {
    my($self, $sent, $received) = @_;
    my($header, $expect, $op, $value);

    $header = $received->header();
    $expect = $self->{expect};
    # check expectations
    foreach my $name (sort(keys(%{ $expect }))) {
        ($op, $value) = @{ $expect->{$name} };
        if (defined($header->{$name})) {
            if (substr($op, -1) eq "=") {
                is($header->{$name}, $value, "header $name");
            } else {
                like($header->{$name}, qr/^($value)$/, "header $name");
            }
            delete($header->{$name}) unless defined($sent->header_field($name));
        } else {
            ok(substr($op, 0, 1) eq "?", "missing header $name");
        }
    }
    # check what remains
    is_deeply($received, $sent, "message " . $sent->header_field("x-uuid"));
}

#
# check the received messages
#

sub check_received ($$) {
    my($self, $sent) = @_;
    my(@messages);

    @messages = MBTF::Support::STOMP::drain($self->{consumer}, $self->{drain});
    unless (@messages == 1) {
        fail(sprintf("received %d message(s)", scalar(@messages)));
        diag(explain(\@messages));
        return;
    }
    compare_messages($self, $sent, $messages[0]);
}

#+##############################################################################
#                                                                              #
# tests                                                                        #
#                                                                              #
#-##############################################################################

__PACKAGE__->add_testinfo("t_empty1", test => "no_plan");
sub t_empty1 : method {
    my($self) = @_;
    my($message);

    $message = Messaging::Message->new();
    send_message($self, $message);
    check_received($self, $message);
}

__PACKAGE__->add_testinfo("t_empty2", test => "no_plan");
sub t_empty2 : method {
    my($self) = @_;
    my($message);

    $message = Messaging::Message->new(
        header => {
            subject => "",
        },
    );
    send_message($self, $message);
    check_received($self, $message);
}

__PACKAGE__->add_testinfo("t_simple", test => "no_plan");
sub t_simple : method {
    my($self) = @_;
    my($message, $string);

    $string = "simple te[sx]t string ;-)";
    $message = Messaging::Message->new(
        text => 1,
        header => {
            subject => $string,
        },
        body => $string,
    );
    send_message($self, $message);
    check_received($self, $message);
}

__PACKAGE__->add_testinfo("t_space1", test => "no_plan");
sub t_space1 : method {
    my($self) = @_;
    my($message, $string);

    $string = " \t<between spaces>\t ";
    $message = Messaging::Message->new(
        text => 1,
        header => {
            subject => $string,
        },
        body => $string,
    );
    send_message($self, $message);
    check_received($self, $message);
}

__PACKAGE__->add_testinfo("t_space2", test => "no_plan");
sub t_space2 : method {
    my($self) = @_;
    my($message);

    $message = Messaging::Message->new(
        header => {
            " foo "   => " spaces ",
            "\tbar\t" => "\ttabs\t",
        },
    );
    send_message($self, $message);
    check_received($self, $message);
}

__PACKAGE__->add_testinfo("t_space3", test => "no_plan");
sub t_space3 : method {
    my($self) = @_;
    my($message);

    $message = Messaging::Message->new(
        header => {
            " "  => " ",
            "\t" => "\t",
        },
    );
    send_message($self, $message);
    check_received($self, $message);
}

__PACKAGE__->add_testinfo("t_printable", test => "no_plan");
sub t_printable : method {
    my($self) = @_;
    my($message, $string);

    $string = join("", grep(/^[[:print:]]$/, map(chr($_ ^ 123), 0 .. 255)));
    $message = Messaging::Message->new(
        text => 1,
        header => {
            subject => $string,
        },
        body => $string,
    );
    send_message($self, $message);
    check_received($self, $message);
}

__PACKAGE__->add_testinfo("t_iso8859", test => "no_plan");
sub t_iso8859 : method {
    my($self) = @_;
    my($message, $string);

    $string = "\xbfLe CERN \xe0 Gen\xe8ve? - Th\xe9\xe2tre fran\xe7ais";
    $message = Messaging::Message->new(
        text => 1,
        header => {
            subject => $string,
        },
        body => $string,
    );
    send_message($self, $message);
    check_received($self, $message);
}

__PACKAGE__->add_testinfo("t_unicode", test => "no_plan");
sub t_unicode : method {
    my($self) = @_;
    my($message, $string);

    $string = "[cedilla=\N{LATIN SMALL LETTER C WITH CEDILLA} - " .
        "sigma=\N{GREEK SMALL LETTER SIGMA} \N{EM DASH} " .
        "smiley=\x{263a}]";
    $message = Messaging::Message->new(
        text => 1,
        header => {
            subject => $string,
        },
        body => $string,
    );
    send_message($self, $message);
    check_received($self, $message);
}

__PACKAGE__->add_testinfo("t_binary", test => "no_plan");
sub t_binary : method {
    my($self) = @_;
    my($message, $string);

    $string = join("", map(chr($_ ^ 123), 0 .. 255));
    $message = Messaging::Message->new(
        text => 0,
        body => $string,
    );
    send_message($self, $message);
    check_received($self, $message);
}

__PACKAGE__->add_testinfo("t_jms", test => "no_plan");
sub t_jms : method {
    my($self) = @_;
    my($message);

    $message = Messaging::Message->new(
        text => 1,
        header => {
            "correlation-id" => "some string",
            "persistent"     => "true",
            "priority"       => "1",
            "reply-to"       => "some destination",
            "type"           => "some text",
        },
        body => "JMS",
    );
    send_message($self, $message);
    check_received($self, $message);
}

__PACKAGE__->add_testinfo("t_weird1text", test => "no_plan");
sub t_weird1text : method {
    my($self) = @_;
    my($message, $string);

    $string = "d\xe9j\xe0";
    $message = Messaging::Message->new(
        text => 1,
        header => {
            $string => $string,
        },
        body => $string,
    );
    send_message($self, $message);
    check_received($self, $message);
}

__PACKAGE__->add_testinfo("t_weird1binary", test => "no_plan");
sub t_weird1binary : method {
    my($self) = @_;
    my($message, $string);

    $string = "d\xe9j\xe0";
    $message = Messaging::Message->new(
        text => 0,
        header => {
            $string => $string,
        },
        body => $string,
    );
    send_message($self, $message);
    check_received($self, $message);
}

__PACKAGE__->add_testinfo("t_weird2", test => "no_plan");
sub t_weird2 : method {
    my($self) = @_;
    my($message);

    $message = Messaging::Message->new(
        header => {
            "a:b"  => "c:d",
            "1\n2" => "3\n4",
            ":"    => "\n",
        },
    );
    send_message($self, $message);
    check_received($self, $message);
}

1;
