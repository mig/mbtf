#+##############################################################################
#                                                                              #
# File: MBTF/Test/Transaction.pm                                               #
#                                                                              #
# Description: test class for transactions                                     #
#                                                                              #
#-##############################################################################

# $Revision: 1166 $

package MBTF::Test::Transaction;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PCxs);
use MBTF::Support::Message qw();
use MBTF::Support::STOMP qw();
use Test::More;

#+##############################################################################
#                                                                              #
# helpers                                                                      #
#                                                                              #
#-##############################################################################

#
# check a synchronous STOMP command
#

sub synchronous ($$%) {
    my($stomp, $command, %option) = @_;

    eval { MBTF::Support::STOMP::synchronous($stomp, $command, %option) };
    if ($@) {
        $@ =~ s/\s+$//;
        fail($@);
    } else {
        pass("synchronous $command");
    }
}

#
# send one message (given its header) and record it
#

sub send_message ($$$) {
    my($test, $messages, $header) = @_;
    my($message, $stomp, $newheader);

    $message = MBTF::Support::Message::random();
    $newheader = { %{ $message->header() }, %{ $header } };
    $message->header($newheader);
    $stomp = $test->{producer};
    $message->header_field(
        "destination" => MBTF::varsub($test->{"producer-destination"}),
    );
    $stomp->send_message($message);
    push(@{ $messages }, $message);
}

#
# receive and check all messages
#

sub receive_messages ($$@) {
    my($test, $regexp, @sent) = @_;
    my(@received, $received, $expected);

    @received = MBTF::Support::STOMP::drain($test->{consumer}, $test->{drain});
    foreach my $message (@received) {
        unless ($message->header_field("x-match") and
                $message->header_field("x-id")) {
            fail("unexpected message received");
            diag(explain($message));
            return;
        }
    }
    $received = @received;
    $expected = grep($_->header_field("x-match"), @sent);
    is($received, $expected, sprintf("received %d message(s)", $received));
    $received = join(":", map($_->header_field("x-id"), @received));
    like($received, $regexp, "received expected message(s)");
}

#
# helper for the two ACK tests (because they are almost identical)
#

sub ack_test ($$) {
    my($test, $what) = @_;
    my($consumer, %option, $tid, @messages, @received);

    return("not relevant for $test->{type}s")
        unless $test->{type} eq "queue";
    return("not relevant for STOMP 1.0")
        if $test->{consumer}->version() eq "1.0";
    # we unsubscribe first because we need to subscribe with client ack
    synchronous($test->{consumer},
                "unsubscribe", id => $test->{"subscribe-id"});
    # we in fact create a new consumer because we will have to disconnect it later
    %option = $test->consumer_option();
    delete(@option{qw(disconnect subscribe)});
    $consumer = MBTF::Support::STOMP::connect(%option);
    %option = %{ MBTF::varsub($test->{"consumer-subscribe"}) };
    $option{ack} = "client-individual";
    MBTF::Support::STOMP::subscribe($consumer, %option);
    # begin the transaction
    $tid = $consumer->uuid();
    synchronous($consumer, "begin", transaction => $tid);
    # send five messages to the queue
    send_message($test, \@messages, { "x-match" => 1, "x-id" => "m1" });
    send_message($test, \@messages, { "x-match" => 1, "x-id" => "m2" });
    send_message($test, \@messages, { "x-match" => 1, "x-id" => "m3" });
    send_message($test, \@messages, { "x-match" => 1, "x-id" => "m4" });
    send_message($test, \@messages, { "x-match" => 1, "x-id" => "m5" });
    # receive and ack all messages but ack the even ones with a transaction
    $consumer->message_callback(sub {
        my($self, $frame) = @_;
        if ($frame->header("x-id") =~ /[02468]$/) {
            $self->ack(frame => $frame, transaction => $tid);
        } else {
            $self->ack(frame => $frame);
        }
        return(1);
    });
    @received = MBTF::Support::STOMP::drain($consumer, $test->{drain});
    is(scalar(@received), scalar(@messages),
       sprintf("received %d message(s)", scalar(@received)));
    # commit|abort the transaction and then disconnect with receipt
    synchronous($consumer, $what, transaction => $tid);
    MBTF::Support::STOMP::disconnect($consumer, receipt => 1);
    # check if we still have messages in the queue
    %option = %{ MBTF::varsub($test->{"consumer-subscribe"}) };
    MBTF::Support::STOMP::subscribe($test->{consumer}, %option);
    @received = MBTF::Support::STOMP::drain($test->{consumer}, $test->{drain});
    if ($what eq "commit") {
        is(scalar(@received), 0, "empty queue after $what");
    } else {
        is(scalar(@received), 2, "non-empty queue after $what");
    }
    return();
}

#+##############################################################################
#                                                                              #
# tests                                                                        #
#                                                                              #
#-##############################################################################

#
# test sending messages and commit
#

__PACKAGE__->add_testinfo("t_send_commit", test => 4);
sub t_send_commit : method {
    my($self) = @_;
    my($tid, @messages);

    $tid = $self->{producer}->uuid();
    # send one message before the transaction
    send_message($self, \@messages, { "x-match" => 1, "x-id" => "m1" });
    # begin the transaction
    synchronous($self->{producer}, "begin", transaction => $tid);
    # send three message within the transaction
    send_message($self, \@messages, { "x-match" => 1, "x-id" => "t1",
                                      transaction => $tid });
    send_message($self, \@messages, { "x-match" => 1, "x-id" => "m2" });
    send_message($self, \@messages, { "x-match" => 1, "x-id" => "t2",
                                      transaction => $tid });
    # commit the transaction
    synchronous($self->{producer}, "commit", transaction => $tid);
    # send one message after the transaction
    send_message($self, \@messages, { "x-match" => 1, "x-id" => "m3" });
    # receive all messages and check them
    receive_messages($self, qr/^m1:m2:t1:t2:m3$/, @messages);
}

#
# test sending messages and abort
#

__PACKAGE__->add_testinfo("t_send_abort", test => 4);
sub t_send_abort : method {
    my($self) = @_;
    my($tid, @messages);

    $tid = $self->{producer}->uuid();
    # send one message before the transaction
    send_message($self, \@messages, { "x-match" => 1, "x-id" => "m1" });
    # begin the transaction
    synchronous($self->{producer}, "begin", transaction => $tid);
    # send three message within the transaction
    send_message($self, \@messages, { "x-match" => 0, "x-id" => "t1",
                                      transaction => $tid });
    send_message($self, \@messages, { "x-match" => 1, "x-id" => "m2" });
    send_message($self, \@messages, { "x-match" => 0, "x-id" => "t2",
                                      transaction => $tid });
    # abort the transaction
    synchronous($self->{producer}, "abort", transaction => $tid);
    # send one message after the transaction
    send_message($self, \@messages, { "x-match" => 1, "x-id" => "m3" });
    # receive all messages and check them
    receive_messages($self, qr/^m1:m2:m3$/, @messages);
}

#
# test acknowledging messages and commit|abort
#

__PACKAGE__->add_testinfo("t_ack_commit", test => 5);
sub t_ack_commit : method {
    my($self) = @_;

    return(ack_test($self, "commit"));
}

__PACKAGE__->add_testinfo("t_ack_abort", test => 5);
sub t_ack_abort : method {
    my($self) = @_;

    return(ack_test($self, "abort"));
}

1;
