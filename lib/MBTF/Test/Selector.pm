#+##############################################################################
#                                                                              #
# File: MBTF/Test/Selector.pm                                                  #
#                                                                              #
# Description: test class for selectors                                        #
#                                                                              #
#-##############################################################################

# $Revision: 2707 $

#
# Options:
#  - field: base field name to use in the expressions (default "foo")
#  - modifier: prefix to use before the expressions (default "")
#

package MBTF::Test::Selector;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PCx);
use MBTF::Support::Message qw();
use MBTF::Support::STOMP qw();
use No::Worries::Die qw(dief);
use Params::Validate qw(validate_with :types);
use Test::More;

#+##############################################################################
#                                                                              #
# constructor                                                                  #
#                                                                              #
#-##############################################################################

my %new_spec = (
    field    => { optional => 1, type => CODEREF | SCALAR | UNDEF },
    modifier => { optional => 1, type => CODEREF | SCALAR | UNDEF },
);

sub new : method {
    my($class, %option);

    $class = shift(@_);
    %option = validate_with(
        params      => \@_,
        spec        => \%new_spec,
        allow_extra => 1,
    );
    $option{field} = "foo" unless defined($option{field});
    $option{modifier} = "" unless defined($option{modifier});
    return(MBTF::super_new($class, __PACKAGE__, \%new_spec, \%option));
}

#+##############################################################################
#                                                                              #
# helpers                                                                      #
#                                                                              #
#-##############################################################################

#
# return the field to use
#

sub get_field ($) {
    my($self) = @_;
    my($test);

    $test = (caller(1))[3];
    return(ref($self->{field}) ? $self->{field}->($test) : $self->{field});
}

#
# create messages
#

sub create_messages (@) {
    my(@headers) = @_;
    my($message, @messages, $newheader);

    foreach my $header (@headers) {
        $message = MBTF::Support::Message::random();
        $newheader = { %{ $message->header() }, %{ $header } };
        $message->header($newheader);
        push(@messages, $message);
    }
    return(@messages);
}

#
# subscribe with selector
#

sub subscribe ($$) {
    my($self, $selector) = @_;
    my(%subopt);

    %subopt = %{ MBTF::varsub($self->{"consumer-subscribe"}) };
    $subopt{selector} = $selector;
    MBTF::Support::STOMP::subscribe($self->{consumer}, %subopt);
}

sub subscribe_good ($$) {
    my($self, $selector) = @_;
    my($test, $modifier);

    $test = (caller(1))[3];
    $modifier = ref($self->{modifier}) ?
        $self->{modifier}->($test) : $self->{modifier};
    $selector = $modifier . $selector;
    subscribe($self, $selector);
    pass("subscribed with selector $selector");
}

sub subscribe_bad ($$) {
    my($self, $selector) = @_;
    my($test, $modifier, $subscription);

    $test = (caller(1))[3];
    $modifier = ref($self->{modifier}) ?
        $self->{modifier}->($test) : $self->{modifier};
    $selector = $modifier . $selector;
    $subscription = "subscription with selector $selector";
    eval {
        subscribe($self, $selector);
    };
    if ($@) {
        $@ =~ s/\s+$//;
        pass("$subscription failed (as expected): $@");
        $self->{consumer_error} = $@;
    } else {
        fail("$subscription succeeded (this is not expected)!");
    }
}

#
# send all messages
#

sub send_messages ($@) {
    my($self, @messages) = @_;
    my($stomp);

    $stomp = $self->{producer};
    foreach my $message (@messages) {
         $message->header_field(
            "destination" => MBTF::varsub($self->{"producer-destination"}),
        );
         $stomp->send_message($message);
    }
    pass(sprintf("sent %d messages", scalar(@messages)));
}

#
# receive and check all messages
#

sub receive_messages ($@) {
    my($self, @sent) = @_;
    my(@received, $received, $expected);

    @received = MBTF::Support::STOMP::drain($self->{consumer}, $self->{drain});
    foreach my $message (@received) {
        unless ($message->header_field("x-match")) {
            fail("unexpected message received");
            diag(explain($message));
            return;
        }
    }
    $received = @received;
    $expected = grep($_->header_field("x-match"), @sent);
    is($received, $expected, sprintf("received %d message(s)", $received));
}

#+##############################################################################
#                                                                              #
# string tests                                                                 #
#                                                                              #
#-##############################################################################

#
# test string equality
#

__PACKAGE__->add_testinfo("t_string_equal", test => 3);
sub t_string_equal : method {
    my($self) = @_;
    my($field, @messages);

    $field = get_field($self);
    subscribe_good($self, "${field} = 'green'");
    @messages = create_messages(
        { "x-match" => 0, $field => "" },
        { "x-match" => 1, $field => "green" },
        { "x-match" => 0, $field => " green" },
        { "x-match" => 0, $field => "green " },
        { "x-match" => 0, $field => "grEEn" },
        { "x-match" => 0, $field => "GREEN" },
    );
    send_messages($self, @messages);
    receive_messages($self, @messages);
}

#
# test string matching
#

__PACKAGE__->add_testinfo("t_string_like", test => 3);
sub t_string_like : method {
    my($self) = @_;
    my($field, @messages);

    $field = get_field($self);
    subscribe_good($self, "${field} LIKE 'gr__n'");
    @messages = create_messages(
        { "x-match" => 0, $field => "" },
        { "x-match" => 1, $field => "green" },
        { "x-match" => 0, $field => " green" },
        { "x-match" => 0, $field => "green " },
        { "x-match" => 1, $field => "grEEn" },
        { "x-match" => 0, $field => "GREEN" },
    );
    send_messages($self, @messages);
    receive_messages($self, @messages);
}

#
# test complex string selector
#

__PACKAGE__->add_testinfo("t_string_complex", test => 3);
sub t_string_complex : method {
    my($self) = @_;
    my($field, @messages);

    $field = get_field($self);
    subscribe_good($self, "(${field} <> 'green') " .
                          "AND (${field} NOT IN ('red', 'pink'))");
    @messages = create_messages(
        { "x-match" => 0, $field => "red" },
        { "x-match" => 0, $field => "green" },
        { "x-match" => 1, $field => "blue" },
        { "x-match" => 0, $field => "pink" },
        { "x-match" => 1, $field => "p!nk" },
    );
    send_messages($self, @messages);
    receive_messages($self, @messages);
}

#+##############################################################################
#                                                                              #
# number tests                                                                 #
#                                                                              #
#-##############################################################################

#
# test number equality
#

__PACKAGE__->add_testinfo("t_number_equal", test => 3);
sub t_number_equal : method {
    my($self) = @_;
    my($field, @messages);

    $field = get_field($self);
    subscribe_good($self, "${field} = 2");
    @messages = create_messages(
        { "x-match" => 0, $field => 1 },
        { "x-match" => 1, $field => 2 },
        { "x-match" => 0, $field => 3 },
        { "x-match" => 0, $field => "two" },
        { "x-match" => 0, $field => " 2" },
        { "x-match" => 0, $field => "2 " },
    );
    send_messages($self, @messages);
    receive_messages($self, @messages);
}

#
# test number matching
#

__PACKAGE__->add_testinfo("t_number_like", test => 3);
sub t_number_like : method {
    my($self) = @_;
    my($field, @messages);

    $field = get_field($self);
    subscribe_good($self, "${field} LIKE '_2_'");
    @messages = create_messages(
        { "x-match" => 0, $field => 111 },
        { "x-match" => 1, $field => 222 },
        { "x-match" => 0, $field => 12 },
        { "x-match" => 0, $field => 21 },
    );
    send_messages($self, @messages);
    receive_messages($self, @messages);
}

#
# test complex number selector
#

__PACKAGE__->add_testinfo("t_number_complex", test => 3);
sub t_number_complex : method {
    my($self) = @_;
    my($field, @messages);

    $field = get_field($self);
    subscribe_good($self, "(${field} >= 2) AND " .
                          "(${field} NOT BETWEEN 3 AND 2+2)");
    @messages = create_messages(
        { "x-match" => 0, $field => 1 },
        { "x-match" => 1, $field => 2 },
        { "x-match" => 0, $field => 3 },
        { "x-match" => 0, $field => 4 },
        { "x-match" => 1, $field => 5 },
    );
    send_messages($self, @messages);
    receive_messages($self, @messages);
}

#+##############################################################################
#                                                                              #
# other expression tests                                                       #
#                                                                              #
#-##############################################################################

#
# test null operators
#

__PACKAGE__->add_testinfo("t_null", test => 3);
sub t_null : method {
    my($self) = @_;
    my($field, @messages);

    $field = get_field($self);
    subscribe_good($self, "(${field}1 IS NULL) AND " .
                          "(${field}2 IS NOT NULL)");
    @messages = create_messages(
        { "x-match" => 0 },
        { "x-match" => 0, "${field}1" => 1 },
        { "x-match" => 1, "${field}2" => 2 },
        { "x-match" => 0, "${field}1" => 1, "${field}2" => 2 },
    );
    send_messages($self, @messages);
    receive_messages($self, @messages);
}

#+##############################################################################
#                                                                              #
# field tests                                                                  #
#                                                                              #
#-##############################################################################

#
# test foo_bar
#

__PACKAGE__->add_testinfo("t_field_underscore", test => 3);
sub t_field_underscore : method {
    my($self) = @_;
    my($field, @messages);

    $field = get_field($self);
    subscribe_good($self, "${field}_bar = 'good'");
    @messages = create_messages(
        { "x-match" => 1, "${field}_bar" => "good" },
        { "x-match" => 0, "${field}_bar" => "bad" },
    );
    send_messages($self, @messages);
    receive_messages($self, @messages);
}

#
# test foo-bar
#

__PACKAGE__->add_testinfo("t_field_dash", test => 3);
sub t_field_dash : method {
    my($self) = @_;
    my($field, @messages);

    $field = get_field($self);
    subscribe_good($self, "${field}-bar = 'good'");
    @messages = create_messages(
        { "x-match" => 1, "${field}-bar" => "good" },
        { "x-match" => 0, "${field}-bar" => "bad" },
    );
    send_messages($self, @messages);
    receive_messages($self, @messages);
}

#+##############################################################################
#                                                                              #
# invalid selectors                                                            #
#                                                                              #
#-##############################################################################

#
# test incomplete
#

__PACKAGE__->add_testinfo("t_fail_incomplete", test => 1);
sub t_fail_incomplete : method {
    my($self) = @_;
    my($field);

    $field = get_field($self);
    subscribe_bad($self, "${field} = ");
}

#
# test foo/bar
#

__PACKAGE__->add_testinfo("t_fail_slash", test => 1);
sub t_fail_slash : method {
    my($self) = @_;
    my($field);

    $field = get_field($self);
    subscribe_bad($self, "${field}/bar");
}

#
# test foo.bar
#

__PACKAGE__->add_testinfo("t_fail_dot", test => 1);
sub t_fail_dot : method {
    my($self) = @_;
    my($field);

    $field = get_field($self);
    subscribe_bad($self, "${field}.bar = 'good'");
}

1;
