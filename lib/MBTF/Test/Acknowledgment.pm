#+##############################################################################
#                                                                              #
# File: MBTF/Test/Acknowledgment.pm                                            #
#                                                                              #
# Description: test class for checking message acknowledgment                  #
#                                                                              #
#-##############################################################################

# $Revision: 1142 $

package MBTF::Test::Acknowledgment;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PC);
use MBTF::Support::STOMP qw();
use Test::More;

#+##############################################################################
#                                                                              #
# helpers                                                                      #
#                                                                              #
#-##############################################################################

#
# send some messages
#

sub send_messages ($$) {
    my($test, $count) = @_;
    my(%option);

    %option = $test->producer_option();
    $option{count} = $count;
    $option{disconnect} = { receipt => 1 };
    MBTF::Support::STOMP::produce(%option);
}

#
# start one consumer
#

sub start_cons ($$) {
    my($test, $ack) = @_;
    my(%option, $cons);

    %option = $test->consumer_option();
    $option{subscribe} = { %{ $option{subscribe} } }; # deep copy
    $option{subscribe}{ack} = $ack;
    $cons = MBTF::Support::STOMP::connect(
        auth     => $option{auth},
        connect  => $option{connect},
        debug    => $option{debug},
        sockopts => $option{sockopts},
        timeout  => $option{timeout},
        uri      => $option{uri},
    );
    MBTF::Support::STOMP::subscribe($cons, $option{subscribe});
    return($cons);
}

#
# stop one consumer
#

sub stop_cons ($$) {
    my($test, $stomp) = @_;

    $stomp->disconnect(receipt => $stomp->uuid());
}

#
# receive one frame
#

sub receive_frame ($$) {
    my($test, $stomp) = @_;

    return($stomp->wait_for_frames(timeout => 1));
}

#
# check the presence of a pending frame
#

sub check_frame ($$) {
    my($test, $stomp) = @_;
    my($frame);

    $frame = receive_frame($test, $stomp);
    ok($frame, "pending frame");
    return($frame);
}

#
# check the absence of pending frames
#

sub check_no_frames ($$) {
    my($test, $stomp) = @_;
    my($frame);

    $frame = receive_frame($test, $stomp);
    ok(!$frame, "no pending frames");
}

#
# check an optional redelivered message
#

sub check_redelivered ($$) {
    my($frame, $cons) = @_;
    my(@seen);

    if ($frame) {
        push(@seen, "redelivered:" .  $frame->header("redelivered"))
            if $frame->header("redelivered");
        push(@seen, "redeliveries:" . $frame->header("redeliveries"))
            if $frame->header("redeliveries");
        pass("message redelivered to $cons (@seen)");
    } else {
        # this is unfortunate but not a violation of the STOMP specification as
        # the broker could have decided to send the message to a DLQ...
        pass("message *not* redelivered to $cons");
    }
}

#+##############################################################################
#                                                                              #
# tests                                                                        #
#                                                                              #
#-##############################################################################

#
# send one message and do not ack it (client) => maybe one redelivered
#

__PACKAGE__->add_testinfo("t_1_client", test => 5);
sub t_1_client : method {
    my($self) = @_;
    my($cons1, $cons2, $frame);

    return("not relevant for $self->{type}s") unless $self->{type} eq "queue";
    # send one test message
    send_messages($self, 1);
    # start one consumer not ack-ing
    $cons1 = start_cons($self, "client");
    $frame = check_frame($self, $cons1) or return("aborted");
    check_no_frames($self, $cons1);
    # start one consumer auto ack-ing
    $cons2 = start_cons($self, "auto");
    check_no_frames($self, $cons2);
    # stop the first consumer
    stop_cons($self, $cons1);
    # check the second consumer
    $frame = receive_frame($self, $cons2);
    check_no_frames($self, $cons2);
    stop_cons($self, $cons2);
    # check redelivered
    check_redelivered($frame, "cons2");
}

#
# send one message and nack it (client) => maybe one redelivered
#

__PACKAGE__->add_testinfo("t_1_client_nack", test => 5);
sub t_1_client_nack : method {
    my($self) = @_;
    my($cons1, $cons2, $frame, $redto);

    return("not relevant for $self->{type}s") unless $self->{type} eq "queue";
    # send one test message
    send_messages($self, 1);
    # start one consumer not ack-ing
    $cons1 = start_cons($self, "client");
    return("not relevant for STOMP 1.0") if $cons1->version() eq "1.0";
    $frame = check_frame($self, $cons1) or return("aborted");
    check_no_frames($self, $cons1);
    # start one consumer auto ack-ing
    $cons2 = start_cons($self, "auto");
    check_no_frames($self, $cons2);
    # nack the message
    $cons1->nack(frame => $frame);
    $frame = receive_frame($self, $cons1);
    if ($frame) {
        # the broker redelivered it to the first consumer!
        $redto = "cons1";
    } else {
        # the broker did not redeliver it to the first consumer
        $redto = "cons2";
        # check the second consumer
        $frame = receive_frame($self, $cons2);
    }
    # stop the second consumer
    check_no_frames($self, $cons2);
    stop_cons($self, $cons2);
    # stop the first consumer
    stop_cons($self, $cons1);
    # check redelivered
    check_redelivered($frame, $redto);
}

#
# send two messages and ack the second only (client) => none redelivered
#

__PACKAGE__->add_testinfo("t_2_client", test => 5);
sub t_2_client : method {
    my($self) = @_;
    my($cons1, $cons2, $frame1, $frame2);

    return("not relevant for $self->{type}s") unless $self->{type} eq "queue";
    # send two test messages
    send_messages($self, 2);
    # start one consumer not ack-ing
    $cons1 = start_cons($self, "client");
    $frame1 = check_frame($self, $cons1) or return("aborted");
    $frame2 = check_frame($self, $cons1) or return("aborted");
    check_no_frames($self, $cons1);
    # start one consumer auto ack-ing
    $cons2 = start_cons($self, "auto");
    check_no_frames($self, $cons2);
    # ack the second message
    $cons1->ack(frame => $frame2);
    # stop the first consumer
    stop_cons($self, $cons1);
    # check the second consumer
    check_no_frames($self, $cons2);
    stop_cons($self, $cons2);
}

#
# send two messages, nack the first one and ack the second only (client) => maybe one redelivered
#

__PACKAGE__->add_testinfo("t_2_client_nack", test => 6);
sub t_2_client_nack : method {
    my($self) = @_;
    my($cons1, $cons2, $frame1, $frame2, $redto);

    return("not relevant for $self->{type}s") unless $self->{type} eq "queue";
    # send two test messages
    send_messages($self, 2);
    # start one consumer not ack-ing
    $cons1 = start_cons($self, "client");
    return("not relevant for STOMP 1.0") if $cons1->version() eq "1.0";
    $frame1 = check_frame($self, $cons1) or return("aborted");
    $frame2 = check_frame($self, $cons1) or return("aborted");
    check_no_frames($self, $cons1);
    # start one consumer auto ack-ing
    $cons2 = start_cons($self, "auto");
    check_no_frames($self, $cons2);
    # nack the first message
    $cons1->nack(frame => $frame1);
    $frame1 = receive_frame($self, $cons1);
    # ack the second message
    $cons1->ack(frame => $frame2);
    if ($frame1) {
        # the broker redelivered it to the first consumer!
        $redto = "cons1";
    } else {
        # the broker did not redeliver it to the first consumer
        $redto = "cons2";
        # check the second consumer
        $frame1 = receive_frame($self, $cons2);
    }
    # stop the second consumer
    check_no_frames($self, $cons2);
    stop_cons($self, $cons2);
    # stop the first consumer
    stop_cons($self, $cons1);
    # check redelivered
    check_redelivered($frame1, $redto);
}

#
# send two messages and ack the second only (client-individual) => maybe one redelivered
#

__PACKAGE__->add_testinfo("t_2_individual", test => 6);
sub t_2_individual : method {
    my($self) = @_;
    my($cons1, $cons2, $frame1, $frame2);

    return("not relevant for $self->{type}s") unless $self->{type} eq "queue";
    # send two test messages
    send_messages($self, 2);
    # start one consumer not ack-ing
    $cons1 = start_cons($self, "client-individual");
    return("not relevant for STOMP 1.0") if $cons1->version() eq "1.0";
    $frame1 = check_frame($self, $cons1) or return("aborted");
    $frame2 = check_frame($self, $cons1) or return("aborted");
    check_no_frames($self, $cons1);
    # start one consumer auto ack-ing
    $cons2 = start_cons($self, "auto");
    check_no_frames($self, $cons2);
    # ack the second message
    $cons1->ack(frame => $frame2);
    # stop the first consumer
    stop_cons($self, $cons1);
    # check the second consumer
    $frame1 = receive_frame($self, $cons2);
    check_no_frames($self, $cons2);
    stop_cons($self, $cons2);
    # check redelivered
    check_redelivered($frame1, "cons2");
}

#
# send two messages, nack the first one and ack the second only (client-individual) => maybe one redelivered
#

__PACKAGE__->add_testinfo("t_2_individual_nack", test => 6);
sub t_2_individual_nack : method {
    my($self) = @_;
    my($cons1, $cons2, $frame1, $frame2, $redto);

    return("not relevant for $self->{type}s") unless $self->{type} eq "queue";
    # send two test messages
    send_messages($self, 2);
    # start one consumer not ack-ing
    $cons1 = start_cons($self, "client-individual");
    return("not relevant for STOMP 1.0") if $cons1->version() eq "1.0";
    $frame1 = check_frame($self, $cons1) or return("aborted");
    $frame2 = check_frame($self, $cons1) or return("aborted");
    check_no_frames($self, $cons1);
    # start one consumer auto ack-ing
    $cons2 = start_cons($self, "auto");
    check_no_frames($self, $cons2);
    # nack the first message
    $cons1->nack(frame => $frame1);
    $frame1 = receive_frame($self, $cons1);
    # ack the second message
    $cons1->ack(frame => $frame2);
    if ($frame1) {
        # the broker redelivered it to the first consumer!
        $redto = "cons1";
    } else {
        # the broker did not redeliver it to the first consumer
        $redto = "cons2";
        # check the second consumer
        $frame1 = receive_frame($self, $cons2);
    }
    # stop the second consumer
    check_no_frames($self, $cons2);
    stop_cons($self, $cons2);
    # stop the first consumer
    stop_cons($self, $cons1);
    # check redelivered
    check_redelivered($frame1, $redto);
}

1;
