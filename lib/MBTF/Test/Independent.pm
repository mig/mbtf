#+##############################################################################
#                                                                              #
# File: MBTF/Test/Independent.pm                                               #
#                                                                              #
# Description: test that the given destinations are independent                #
#                                                                              #
#-##############################################################################

# $Revision: 1054 $

package MBTF::Test::Independent;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PCxs);
use MBTF::Support::Message qw();
use MBTF::Support::STOMP qw();
use Test::More;

#+##############################################################################
#                                                                              #
# tests                                                                        #
#                                                                              #
#-##############################################################################

__PACKAGE__->add_testinfo("t_send_receive", test => 2);
sub t_send_receive : method {
    my($self) = @_;
    my($stomp, $message, @messages);

    # send one random message
    $stomp = $self->{producer};
    $message = MBTF::Support::Message::random();
    $message->header_field(
        "destination" => MBTF::varsub($self->{"producer-destination"}),
    );
    $stomp->send_message($message);
    pass("sent test message");
    # (hopefully) do not receive it
    @messages = MBTF::Support::STOMP::drain($self->{consumer}, $self->{drain});
    if (@messages) {
        fail(sprintf("received %d message(s)", scalar(@messages)));
    } else {
        pass("did not receive test message (as expected)");
    }
}

1;
