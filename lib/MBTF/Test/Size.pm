#+##############################################################################
#                                                                              #
# File: MBTF/Test/Size.pm                                                      #
#                                                                              #
# Description: test class for size-based tests                                 #
#                                                                              #
#-##############################################################################

# $Revision: 1054 $

package MBTF::Test::Size;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PCxs);
use Messaging::Message qw();
use MBTF::Support::STOMP qw();
use Test::More;

#+##############################################################################
#                                                                              #
# helpers                                                                      #
#                                                                              #
#-##############################################################################

#
# send then receive one message
#

sub send_receive_message ($$$$) {
    my($test, $header, $body, $descro) = @_;
    my($message, $stomp, $uuid, @received, $tmp);

    # send one message
    $header ||= {};
    $body ||= "";
    $message = Messaging::Message->new(header => $header, body => $body);
    $stomp = $test->{producer};
    $uuid = $stomp->uuid();
    $message->header_field(
        "destination" => MBTF::varsub($test->{"producer-destination"}),
    );
    $message->header_field("x-uuid" => $uuid);
    $stomp->send_message($message);
    # receive it back (hopefully)
    @received = MBTF::Support::STOMP::drain($test->{consumer}, $test->{drain});
    $tmp = @received == 1 ?
        $received[0]->header_field("x-uuid") : "received=" . @received;
    is($tmp, $uuid, "sent+received one message ($descro)");
}

#
# define one body test
#

sub define_body_test ($) {
    my($k) = @_;
    my($test);

    $test = sprintf("t_body_%03dk", $k);
    __PACKAGE__->add_testinfo($test, test => 1);
    no strict "refs";
    *{__PACKAGE__ . "::${test}"} = sub {
        my($self) = @_;
        send_receive_message($self, {}, "A" x (1024 * $k), "${k}k body");
    };
}

#+##############################################################################
#                                                                              #
# tests                                                                        #
#                                                                              #
#-##############################################################################

#
# test sending and receiving one message (with different body sizes)
#

foreach my $n (0 .. 9) {
    define_body_test(1 << $n);
}

1;
