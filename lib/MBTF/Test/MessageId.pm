#+##############################################################################
#                                                                              #
# File: MBTF/Test/MessageId.pm                                                 #
#                                                                              #
# Description: test class for checking the message-id header                   #
#                                                                              #
#-##############################################################################

# $Revision: 2708 $

package MBTF::Test::MessageId;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use threads;
use threads::shared;
use base qw(MBTF::Base::PC);
use List::Util qw(sum);
use MBTF::Support::STOMP qw();
use MBTF::Support::Thread qw();
use Test::More;

#
# constants
#

use constant MESSAGE_ID_VALUE => "dummy";
use constant MESSAGE_ID_RE    => qr/^dummy\d+$/;

#+##############################################################################
#                                                                              #
# helpers                                                                      #
#                                                                              #
#-##############################################################################

#
# start one sequential producer for N messages
#

sub start_prod ($$) {
    my($test, $count) = @_;
    my(%option, $i);

    %option = $test->producer_option();
    $option{count} = $count;
    $option{disconnect} = { receipt => 1 };
    $option{callback} = sub {
        my($msg) = @_;
        $msg->header_field("message-id", MESSAGE_ID_VALUE . $i++);
    };
    $i = 1;
    MBTF::Support::STOMP::produce(%option);
}

#
# start N parallel consumers with semaphore
#

sub start_cons ($$$) {
    my($test, $count, $seen) = @_;
    my(%option, $name);

    %option = $test->consumer_option();
    $option{drain} = $test->{drain};
    $option{semaphore} = 1;
    $option{subscribe} = { %{ $option{subscribe} } }; # deep copy
    $option{subscribe}{ack} = "client";
    $option{callback} = sub {
        my($msg) = @_;
        lock($seen);
        $seen->{$msg->header_field("message-id")}++;
    };
    foreach my $i (1 .. $count) {
        $name = "cons$i";
        MBTF::Support::Thread::create($name, \&MBTF::Support::STOMP::consume,
                                      %option);
    }
    $MBTF::Support::STOMP::Subscribed->down($count);
    $MBTF::Support::STOMP::Started->up($count);
}

#
# start one drainer
#

sub start_drain ($) {
    my($test) = @_;
    my(%option, $count);

    %option = $test->consumer_option();
    $option{drain} = $test->{drain};
    $count = MBTF::Support::STOMP::consume(%option);
    return($count);
}

#+##############################################################################
#                                                                              #
# tests                                                                        #
#                                                                              #
#-##############################################################################

__PACKAGE__->add_testinfo("t_ack_count", test => 4);
sub t_ack_count : method {
    my($self) = @_;
    my(%seen, $tmp);

    share(%seen);
    # start two consumers
    start_cons($self, 2, \%seen);
    # send two messages with dummy message-id
    start_prod($self, 2);
    # wait for consumers to finish
    MBTF::Support::Thread::wait4all();
    # check what we received
    $tmp = keys(%seen);
    cmp_ok($tmp, ">=", 2, "seen $tmp message-id's");
    $tmp = sum(values(%seen));
    if ($self->{type} eq "queue") {
        is($tmp, 2, "received 2 messages");
    } else {
        is($tmp, 4, "received 4 messages");
    }
    $tmp = join("|", sort(grep($_ !~ MESSAGE_ID_RE, keys(%seen))));
    if ($tmp) {
        pass("user-supplied message-id is overwritten ($tmp)");
    } else {
        pass("user-supplied message-id is preserved");
    }
    # drain
    if ($self->{type} eq "queue") {
        $tmp = start_drain($self);
        is($tmp, 0, "no messages left in $self->{type}");
    } else {
        return("no draining for $self->{type}s");
    }
}

1;
