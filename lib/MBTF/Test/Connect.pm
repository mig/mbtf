#+##############################################################################
#                                                                              #
# File: MBTF/Test/Connect.pm                                                   #
#                                                                              #
# Description: wrapper module for connection tests (no destinations used)      #
#                                                                              #
#-##############################################################################

# $Revision: 2294 $

package MBTF::Test::Connect;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use MBTF::Test::Connect::Good qw();
use MBTF::Test::Connect::Bad qw();

#
# instantiate all tests
#

sub test_all (%) {
    my(%option) = @_;
    my($good, $bad, %newopt, %conopt);

    $good = delete($option{good});
    $bad = delete($option{bad});
    if ($good) {
        foreach my $auth (@{ $good }) {
            # first try with the given options
            %newopt = %option;
            $newopt{auth} = $auth;
            MBTF::Test::Connect::Good->new(%newopt);
            # then try forcing the STOMP version
            delete($newopt{connect});
            %conopt = $option{connect} ? %{ $option{connect} } : ();
            delete($conopt{version});
            foreach my $version (qw(1.0 1.1 1.2)) {
                $newopt{connect} = { %conopt, version => $version };
                MBTF::Test::Connect::Good->new(%newopt);
            }
        }
        # then try with a bogus STOMP version (last good auth only)
        $newopt{connect} = { %conopt, version => "3.14" };
        MBTF::Test::Connect::Bad->new(%newopt);
    }
    if ($bad) {
        foreach my $auth (@{ $bad }) {
            %newopt = %option;
            $newopt{auth} = $auth;
            MBTF::Test::Connect::Bad->new(%newopt);
        }
    }
}

1;
