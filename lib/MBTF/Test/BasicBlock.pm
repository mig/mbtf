#+##############################################################################
#                                                                              #
# File: MBTF/Test/BasicBlock.pm                                                #
#                                                                              #
# Description: test class for testing our messaging basic block                #
#                                                                              #
#-##############################################################################

# $Revision: 1020 $

#
# Options:
#  - dsub: code reference to create durable subscriptions
#

package MBTF::Test::BasicBlock;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PC);
use MBTF::Support::Client qw();
use MBTF::Support::STOMP qw();
use MBTF::Test::Count qw();
use Params::Validate qw(validate_with :types);
use Test::More;

#+##############################################################################
#                                                                              #
# constructor                                                                  #
#                                                                              #
#-##############################################################################

my %new_spec = (
    "dsub" => { optional => 0, type => CODEREF },
);

sub new : method {
    my($class, %option);

    $class = shift(@_);
    %option = validate_with(
        params      => \@_,
        spec        => \%new_spec,
        allow_extra => 1,
    );
    return(MBTF::super_new($class, __PACKAGE__, \%new_spec, \%option));
}

#+##############################################################################
#                                                                              #
# helpers                                                                      #
#                                                                              #
#-##############################################################################

#
# create an MBTF::Test::Count test object
#

sub create_count_test ($%) {
    my($test, %extra) = @_;
    my(%option);

    %option = (%{ $test }, %extra);
    delete($option{dsub});
    $option{"producer-count"} ||= 3;
    $option{"consumer-count"} ||= 3;
    return(MBTF::Test::Count->new(%option));
}

#
# drain the destination
#

sub drain ($$) {
    my($test, $result) = @_;

    MBTF::Support::Client::run_drainers($test, $test->consumer_option(),
        "drain"  => $test->{"drain"},
        "result" => $result,
    );
}

#+##############################################################################
#                                                                              #
# tests                                                                        #
#                                                                              #
#-##############################################################################

__PACKAGE__->add_testinfo("t_basic_block", test => 16);
sub t_basic_block : method {
    my($self) = @_;
    my($test, %count, %qsub1, %qsub2);

    return("not relevant for $self->{type}s") unless $self->{type} eq "topic";
    #
    # part 1: the given destination should behave like a topic
    #
    $test = create_count_test($self, "type" => "topic");
    $test->t_count_pp_pd();
    $test->t_count_pd_pp();
    #
    # part 2: the created queues should be empty
    #
    %qsub1 = $self->{dsub}->($self, "q1");
    {
        local $self->{"consumer-subscribe"} = \%qsub1;
        drain($self, \%count);
        is($count{"drain"}, 0, "empty queue 1");
    }
    %qsub2 = $self->{dsub}->($self, "q2");
    {
        local $self->{"consumer-subscribe"} = \%qsub2;
        drain($self, \%count);
        is($count{"drain"}, 0, "empty queue 2");
    }
    #
    # part 3: the first queue should behave like a queue
    #
    $count{"produced"} = 0;
    {
        local $self->{"consumer-subscribe"} = \%qsub1;
        $test = create_count_test($self, "type" => "queue");
        $count{"produced"} += $test->t_count_pp_pd();
        $count{"produced"} += $test->t_count_pd_pp();
    }
    #
    # part 4: the second queue should contain all the messages produced
    #
    {
        local $self->{"consumer-subscribe"} = \%qsub2;
        drain($self, \%count);
        is($count{"drain"}, $count{"produced"}, "filled queue 2");
    }
    #
    # part 5: the created queue can maybe be used directly as a queue
    #
    return("no direct queue access") unless keys(%qsub1) == 1;
    {
        local $self->{"producer-destination"} = $qsub1{destination};
        local $self->{"consumer-subscribe"} = \%qsub1;
        $test = create_count_test($self, "type" => "queue");
        $count{"produced"} += $test->t_count_pp_pd();
        $count{"produced"} += $test->t_count_pd_pp();
    }
    {
        local $self->{"consumer-subscribe"} = \%qsub2;
        drain($self, \%count);
        is($count{"drain"}, 0, "empty queue 2");
    }
}

1;
