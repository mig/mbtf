#+##############################################################################
#                                                                              #
# File: MBTF/Test/Count.pm                                                     #
#                                                                              #
# Description: test class for counting messages exchanged by clients           #
#                                                                              #
#-##############################################################################

# $Revision: 1020 $

#
# Options:
#  - (producer|consumer)-count: the number of producers|consumers (optional)
#

package MBTF::Test::Count;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use threads;
use base qw(MBTF::Base::PCd);
use MBTF::Support::Client qw();
use MBTF::Support::STOMP qw();
use Params::Validate qw(validate_with :types);
use Test::More;

#+##############################################################################
#                                                                              #
# constructor                                                                  #
#                                                                              #
#-##############################################################################

# here is what can be given to new():

my %new_spec1 = (
    "producer-count" => { optional => 1, type => SCALAR | UNDEF },
    "consumer-count" => { optional => 1, type => SCALAR | UNDEF },
);

# here is what will appear in the object:

my %new_spec2 = (
    "producer-count" => { optional => 0, type => SCALAR, regex => qr/^\d+$/ },
    "consumer-count" => { optional => 0, type => SCALAR, regex => qr/^\d+$/ },
);

sub new : method {
    my($class, %option, $object);

    $class = shift(@_);
    %option = validate_with(
        params      => \@_,
        spec        => \%new_spec1,
        allow_extra => 1,
    );
    # defaults
    $option{"producer-count"} ||= 1;
    $option{"consumer-count"} ||= 1;
    # create the object
    $object = MBTF::super_new($class, __PACKAGE__, \%new_spec2, \%option);
    # re-check for mandatory options
    validate_with(
        params      => [ %{ $object } ],
        spec        => \%new_spec2,
        allow_extra => 1,
    );
    # so far so good
    return($object);
}

#+##############################################################################
#                                                                              #
# tests                                                                        #
#                                                                              #
#-##############################################################################

__PACKAGE__->add_testinfo("t_count_pp_sd", test => 2);
sub t_count_pp_sd : method {
    my($self) = @_;
    my(%count);

    # parallel producers
    MBTF::Support::Client::start_producers($self, $self->producer_option(),
        duration => $self->{duration},
        clients  => $self->{"producer-count"},
    );
    MBTF::Support::Client::wait_all($self, result => \%count) or return();
    # sequential drainer (only one)
    MBTF::Support::Client::run_drainers($self, $self->consumer_option(),
        drain  => $self->{drain},
        result => \%count,
    );
    # check
    $count{produced} = MBTF::Support::Client::sum_produced(\%count);
    cmp_ok($count{produced}, ">", 0, "sum(produced) > 0");
    if ($self->{type} eq "queue") {
        is($count{drain}, $count{produced}, "sum(drained) == sum(produced)");
    } else {
        is($count{drain}, 0, "sum(drained) == 0");
    }
    # return what has been produced
    return($count{produced});
}

__PACKAGE__->add_testinfo("t_count_pp_pd", test => 2);
sub t_count_pp_pd : method {
    my($self) = @_;
    my(%count);

    # parallel producers
    MBTF::Support::Client::start_producers($self, $self->producer_option(),
        duration => $self->{duration},
        clients  => $self->{"producer-count"},
    );
    MBTF::Support::Client::wait_all($self, result => \%count) or return();
    # parallel drainers
    MBTF::Support::Client::start_drainers($self, $self->consumer_option(),
        drain     => $self->{drain},
        semaphore => 0,
        clients   => $self->{"consumer-count"},
    );
    MBTF::Support::Client::wait_all($self, result => \%count) or return();
    # check
    $count{produced} = MBTF::Support::Client::sum_produced(\%count);
    $count{drained} = MBTF::Support::Client::sum_drained(\%count);
    cmp_ok($count{produced}, ">", 0, "sum(produced) > 0");
    if ($self->{type} eq "queue") {
        is($count{drained}, $count{produced}, "sum(drained) == sum(produced)");
    } else {
        is($count{drained}, 0, "sum(drained) == 0");
    }
    # return what has been produced
    return($count{produced});
}

__PACKAGE__->add_testinfo("t_count_pd_sp", test => 2);
sub t_count_pd_sp : method {
    my($self) = @_;
    my(%count);

    # parallel drainers
    MBTF::Support::Client::start_drainers($self, $self->consumer_option(),
        drain     => $self->{drain},
        semaphore => 1,
        clients   => $self->{"consumer-count"},
    );
    $MBTF::Support::STOMP::Subscribed->down($self->{"consumer-count"});
    $MBTF::Support::STOMP::Started->up($self->{"consumer-count"});
    # sequential producers
    MBTF::Support::Client::run_producers($self, $self->producer_option(),
        duration => $self->{duration},
        clients  => $self->{"producer-count"},
        result   => \%count,
    );
    MBTF::Support::Client::wait_all($self, result => \%count) or return();
    # check
    $count{produced} = MBTF::Support::Client::sum_produced(\%count);
    $count{drained} = MBTF::Support::Client::sum_drained(\%count);
    cmp_ok($count{produced}, ">", 0, "sum(produced) > 0");
    if ($self->{type} eq "queue") {
        is($count{drained}, $count{produced}, "sum(drained) == sum(produced)");
    } else {
        is(
            join(" ", map($count{$_}, grep(/^drain\d*$/, keys(%count)))),
            join(" ", ($count{produced}) x $self->{"consumer-count"}),
            "each(drained) == sum(produced)",
        );
    }
    # return what has been produced
    return($count{produced});
}

__PACKAGE__->add_testinfo("t_count_pd_pp", test => 2);
sub t_count_pd_pp : method {
    my($self) = @_;
    my(%count);

    # parallel drainers
    MBTF::Support::Client::start_drainers($self, $self->consumer_option(),
        drain     => $self->{drain},
        semaphore => 1,
        clients   => $self->{"consumer-count"},
    );
    $MBTF::Support::STOMP::Subscribed->down($self->{"consumer-count"});
    # parallel producers
    MBTF::Support::Client::start_producers($self, $self->producer_option(),
        duration => $self->{duration},
        clients  => $self->{"producer-count"},
    );
    $MBTF::Support::STOMP::Started->up($self->{"consumer-count"});
    MBTF::Support::Client::wait_all($self, result => \%count) or return();
    # check
    $count{produced} = MBTF::Support::Client::sum_produced(\%count);
    $count{drained} = MBTF::Support::Client::sum_drained(\%count);
    cmp_ok($count{produced}, ">", 0, "sum(produced) > 0");
    if ($self->{type} eq "queue") {
        is($count{drained}, $count{produced}, "sum(drained) == sum(produced)");
    } else {
        is(
            join(" ", map($count{$_}, grep(/^drain\d*$/, keys(%count)))),
            join(" ", ($count{produced}) x $self->{"consumer-count"}),
            "each(drained) == sum(produced)",
        );
    }
    # return what has been produced
    return($count{produced});
}

__PACKAGE__->add_testinfo("t_count_pp_sc", test => 3);
sub t_count_pp_sc : method {
    my($self) = @_;
    my(%count);

    return("not relevant for $self->{type}s") unless $self->{type} eq "queue";
    # parallel producers
    MBTF::Support::Client::start_producers($self, $self->producer_option(),
        duration => $self->{duration},
        clients  => $self->{"producer-count"},
    );
    MBTF::Support::Client::wait_all($self, result => \%count) or return();
    # sequential consumers
    MBTF::Support::Client::run_consumers($self, $self->consumer_option(),
        messages => MBTF::Support::Client::sum_produced(\%count),
        clients  => $self->{"consumer-count"},
        result   => \%count,
    );
    # final drainer
    MBTF::Support::Client::run_drainers($self, $self->consumer_option(),
        drain  => $self->{drain},
        result => \%count,
    );
    # check
    $count{produced} = MBTF::Support::Client::sum_produced(\%count);
    $count{consumed} = MBTF::Support::Client::sum_consumed(\%count);
    cmp_ok($count{produced}, ">", 0, "sum(produced) > 0");
    is($count{consumed}, $count{produced}, "sum(consumed) == sum(produced)");
    is($count{drain}, 0, "drained == 0");
    # return what has been produced
    return($count{produced});
}

__PACKAGE__->add_testinfo("t_count_pp_pc", test => 3);
sub t_count_pp_pc : method {
    my($self) = @_;
    my(%count);

    return("not relevant for $self->{type}s") unless $self->{type} eq "queue";
    # parallel producers
    MBTF::Support::Client::start_producers($self, $self->producer_option(),
        duration => $self->{duration},
        clients  => $self->{"producer-count"},
    );
    MBTF::Support::Client::wait_all($self, result => \%count) or return();
    # parallel consumers
    MBTF::Support::Client::start_consumers($self, $self->consumer_option(),
        messages => MBTF::Support::Client::sum_produced(\%count),
        clients  => $self->{"consumer-count"},
    );
    MBTF::Support::Client::wait_all($self, result => \%count) or return();
    # final drainer
    MBTF::Support::Client::run_drainers($self, $self->consumer_option(),
        drain  => $self->{drain},
        result => \%count,
    );
    # check
    $count{produced} = MBTF::Support::Client::sum_produced(\%count);
    $count{consumed} = MBTF::Support::Client::sum_consumed(\%count);
    cmp_ok($count{produced}, ">", 0, "sum(produced) > 0");
    is($count{consumed}, $count{produced}, "sum(consumed) == sum(produced)");
    is($count{drain}, 0, "drained == 0");
    # return what has been produced
    return($count{produced});
}

1;
