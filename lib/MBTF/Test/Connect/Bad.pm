#+##############################################################################
#                                                                              #
# File: MBTF/Test/Connect/Bad.pm                                               #
#                                                                              #
# Description: test class for unsuccessful connections                         #
#                                                                              #
#-##############################################################################

# $Revision: 1142 $

package MBTF::Test::Connect::Bad;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::C);
use MBTF::Support::STOMP qw();
use Test::More;

#
# attempt to connect and expect a failure
#

__PACKAGE__->add_testinfo("t_connect", test => 1);
sub t_connect : method {
    my($self) = @_;
    my($stomp);

    # Note: we use handleref to make sure the object does not get destroyed
    # since this could overwrite $@...
    eval {
        MBTF::Support::STOMP::connect(
            auth      => $self->{"auth"},
            connect   => $self->{"connect"},
            debug     => $self->{"stomp-debug"},
            sockopts  => $self->{"sockopts"},
            uri       => $self->{"uri"},
            handleref => \$stomp,
        );
    };
    if ($@) {
        $@ =~ s/\s+$//;
        pass("connection to $self->{uri} failed (as expected): $@");
    } else {
        fail("connection to $self->{uri} succeeded (this is not expected)!");
    }
}

1;
