#+##############################################################################
#                                                                              #
# File: MBTF/Test/Connect/Good.pm                                              #
#                                                                              #
# Description: test class for successful connections                           #
#                                                                              #
#-##############################################################################

# $Revision: 2570 $

package MBTF::Test::Connect::Good;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::Cx);
use Test::More;

#
# check the server of the CONNECTED frame
#

__PACKAGE__->add_testinfo("t_server", test => 1);
sub t_server : method {
    my($self) = @_;
    my($server);

    $server = $self->{client}->server();
    if (not defined($server)) {
        # ok since it's optional in the spec...
        pass("undefined server");
    } elsif ($server =~ /^ActiveMQ\//) {
        pass("ActiveMQ server: $server");
    } elsif ($server =~ /^apache-apollo\//) {
        pass("Apollo server: $server");
    } elsif ($server =~ /^ActiveMQ-Artemis\//) {
        pass("Artemis server: $server");
    } elsif ($server =~ /^RabbitMQ\//) {
        pass("RabbitMQ server: $server");
    } else {
        fail("unknown server: $server");
    }
}

#
# check the version of the CONNECTED frame (single version only)
#

__PACKAGE__->add_testinfo("t_version", test => 1);
sub t_version : method {
    my($self) = @_;
    my($version, $convers);

    $version = $self->{client}->version();
    $convers = $self->{connect}{version} || "*";
    if ($convers eq "*" or $version eq $convers) {
        pass("STOMP version: asked $convers got $version");
    } else {
        fail("STOMP version: asked $convers got $version");
    }
}

1;
