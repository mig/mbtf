#+##############################################################################
#                                                                              #
# File: MBTF/Test/Expiration.pm                                                #
#                                                                              #
# Description: test class for checking message expiration                      #
#                                                                              #
#-##############################################################################

# $Revision: 1142 $

package MBTF::Test::Expiration;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(MBTF::Base::PC);
use MBTF::Support::STOMP qw();
use No::Worries::Die qw(dief);
use Params::Validate qw(validate_with :types);
use Test::More;

#+##############################################################################
#                                                                              #
# constructor                                                                  #
#                                                                              #
#-##############################################################################

# here is what can be given to new():

my %new_spec1 = (
    "expiration-header" => { optional => 1, type => SCALAR | UNDEF },
    "expiration-type"   => { optional => 1, type => SCALAR | UNDEF },
);

# here is what will appear in the object:

my %new_spec2 = (
    "expiration-header" => {
        optional => 0,
        type     => SCALAR,
        regex    => qr/^([a-z]+)$/,
    },
    "expiration-type" => {
        optional => 0,
        type     => SCALAR,
        regex    => qr/^(time|ttl)$/,
    },
);

sub new : method {
    my($class, %option, $object);

    $class = shift(@_);
    %option = validate_with(
        params      => \@_,
        spec        => \%new_spec1,
        allow_extra => 1,
    );
    # defaults
    $option{"expiration-header"} ||= "expires";
    $option{"expiration-type"} ||= "time";
    # create the object
    $object = MBTF::super_new($class, __PACKAGE__, \%new_spec2, \%option);
    # re-check for mandatory options
    validate_with(
        params      => [ %{ $object } ],
        spec        => \%new_spec2,
        allow_extra => 1,
    );
    # so far so good
    return($object);
}

#+##############################################################################
#                                                                              #
# helpers                                                                      #
#                                                                              #
#-##############################################################################

#
# start one consumer
#

sub start_cons ($) {
    my($test) = @_;
    my(%option, $cons);

    %option = $test->consumer_option();
    $cons = MBTF::Support::STOMP::connect(
        auth     => $option{auth},
        connect  => $option{connect},
        debug    => $option{debug},
        sockopts => $option{sockopts},
        timeout  => $option{timeout},
        uri      => $option{uri},
    );
    MBTF::Support::STOMP::subscribe($cons, $option{subscribe});
    return($cons);
}

#
# stop one consumer
#

sub stop_cons ($$) {
    my($test, $stomp) = @_;

    $stomp->disconnect(receipt => $stomp->uuid());
}

#
# send some messages
#

sub send_messages ($$$) {
    my($test, $count, $callback) = @_;
    my(%option);

    %option = $test->producer_option();
    $option{count} = $count;
    $option{callback} = $callback;
    $option{disconnect} = { receipt => 1 };
    MBTF::Support::STOMP::produce(%option);
}

#
# receive all messages
#

sub receive_messages ($$) {
    my($test, $stomp) = @_;
    my($frame, @received);

    while (1) {
        $frame = $stomp->wait_for_frames(timeout => 1);
        last unless $frame;
        dief("unexpected frame received: %s", $frame->command())
            unless $frame->command() eq "MESSAGE";
        push(@received, $frame->messagify());
    }
    return(@received);
}

#+##############################################################################
#                                                                              #
# tests                                                                        #
#                                                                              #
#-##############################################################################

__PACKAGE__->add_testinfo("t_expired", test => 1);
sub t_expired : method {
    my($self) = @_;
    my($cons, $time, $count);

    # start one consumer
    $cons = start_cons($self);
    # send some expired messages
    if ($self->{"expiration-type"} eq "time") {
        # find the current time (and rely on the broker clock to be in sync)
        $time = time() * 1000;
        # substract one second to be sure the time is in the past
        $time -= 1000;
        send_messages($self, 3, sub {
            my($msg) = @_;
            $msg->header_field($self->{"expiration-header"}, $time);
            $time -= int(rand(1000)) * 1000;
        });
    } else {
        # header will contain the TTL
        $time = -1;
        send_messages($self, 3, sub {
            my($msg) = @_;
            $msg->header_field($self->{"expiration-header"}, $time);
            $time -= int(rand(1000)) * 1000;
        });
    }
    # check how many we received
    $count = receive_messages($self, $cons);
    is($count, 0, "received no expired messages");
    stop_cons($self, $cons);
}

__PACKAGE__->add_testinfo("t_expiration", test => 1);
sub t_expiration : method {
    my($self) = @_;
    my($cons, $time, $count, @expires);

    return("not relevant for $self->{type}s") unless $self->{type} eq "queue";
    if ($self->{"expiration-type"} eq "time") {
        # find the current time (and rely on the broker clock to be in sync)
        $time = time() * 1000;
        # send some messages with or without expiration
        @expires = (
            $time - 1000,       # in the past: already expired
            0,                  # no expires header
            $time + 30000,      # +30s: will not expire
            $time + 3000,       # +3s: will expire
            $time + 300,        # +0.3s: will expire
        );
    } else {
        # header will contain the TTL
        @expires = (
            0,                  # no expires header
            30000,              # +30s: will not expire
            3000,               # +3s: will expire
            300,                # +0.3s: will expire
        );
    }
    send_messages($self, scalar(@expires), sub {
        my($msg) = @_;
        $time = shift(@expires);
        $msg->header_field($self->{"expiration-header"}, $time) if $time;
    });
    # wait a bit to make sure two messages expire
    sleep(4);
    # start one consumer and check how many we received
    $cons = start_cons($self);
    $count = receive_messages($self, $cons);
    is($count, 2, "received no expired messages");
    stop_cons($self, $cons);
}

1;
