#+##############################################################################
#                                                                              #
# File: MBTF/Base.pm                                                           #
#                                                                              #
# Description: base class for MBTF (all other classes must inherit from it)    #
#                                                                              #
#-##############################################################################

# $Revision: 1054 $

#
# Options:
#  - debug: true if the debug method should log something
#  - stomp-debug: the debug flags for Net::STOMP::Client
#
# Methods:
#  - debug: log something if the debug option is true
#  - omit: omit the given method (via Test::Class filtering)
#

package MBTF::Base;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use base qw(Test::Class);
use MBTF qw();
use No::Worries::Die qw(dief);
use No::Worries::Log qw(log_debug);
use Params::Validate qw(validate :types);

#
# global variables
#

our(
    @Tests,                        # the list of all test objects ever created
    %Omit,                        # the hash of class+method to be omitted
);

#
# constructor
#

my %new_spec = (
    "debug"       => { type => SCALAR },
    "stomp-debug" => { type => SCALAR },
);
MBTF::spec_optional(\%new_spec, keys(%new_spec));

sub new : method {
    my($class, %option, $object);

    $class = shift(@_);
    %option = validate(@_, \%new_spec) if @_;
    MBTF::option_numerical(\%option, qw(debug));
    MBTF::option_string(\%option, qw(stomp-debug));
    $object = MBTF::super_new($class, __PACKAGE__, \%new_spec, \%option);
    push(@Tests, $object);
    return($object);
}

#
# debug method
#

sub debug : method {
    my($self, $message, @arguments) = @_;

    log_debug($message, @arguments, { what => "test" }) if $self->{"debug"};
}

#
# omit method
#

sub omit : method {
    my($self, @methods) = @_;
    my($class);

    $class = ref($self) || $self;
    foreach my $method (@methods) {
        dief("unknown method: %s->%s()", $class, $method)
            unless $class->can($method);
        $Omit{$class}{$method}++;
    }
    return($self); # allow chaining
}

#
# module initialization
#

Test::Class->add_filter(sub {
    my($class, $method) = @_;
    return(0) if $Omit{$class} and $Omit{$class}{$method};
    return(1);
});

1;
