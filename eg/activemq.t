#
# run all standard tests for an ActiveMQ broker
#

use Authen::Credential qw();
use MBTF::ActiveMQ qw();

MBTF::ActiveMQ::test_all(
    uri  => "stomp://mybroker:6163",
    auth => Authen::Credential->parse("none"),
);
