#
# run all standard tests for a RabbitMQ broker
#

use Authen::Credential qw();
use MBTF::RabbitMQ qw();

MBTF::RabbitMQ::test_all(
    uri  => "stomp://mybroker:6163",
    connect => { host => "myvhost" }),
    auth => Authen::Credential->parse("none"),
);
