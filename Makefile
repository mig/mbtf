PKGNAME=mbtf
PKGVERSION=$(shell cat VERSION)
PKGTAG=v${PKGVERSION}
FULLPKGNAME=${PKGNAME}-${PKGVERSION}
DISTDIR=dist/${FULLPKGNAME}
TARBALL=${FULLPKGNAME}.tgz
BINS=mbtf

INSTROOT=
BINDIR=/usr/bin
MANDIR=/usr/share/man
TOPDIR=/opt/${PKGNAME}

.PHONY: build install tag sources rpm srpm clean

build:
	mkdir man
	@for name in ${BINS}; do \
	  pod2man --section=1 bin/$$name > man/$$name.1; \
	done

install:
	@for path in `find lib -type f`; do \
	  install -D -m 644 $$path ${INSTROOT}/${TOPDIR}/$$path; \
	done
	@for path in `find bin -type f`; do \
	  install -D -m 755 $$path ${INSTROOT}/${TOPDIR}/$$path; \
	done
	mkdir -p ${INSTROOT}/${BINDIR}
	@for name in ${BINS}; do \
	  ln -s ${TOPDIR}/bin/$$name ${INSTROOT}/${BINDIR}/$$name; \
	  install -D -m 644 man/$$name.1 ${INSTROOT}${MANDIR}/man1/$$name.1; \
	done

tag:
	@seen=`git tag -l | grep -Fx ${PKGTAG}`; \
	if [ "x$$seen" = "x" ]; then \
	    set -x; \
	    git tag ${PKGTAG}; \
	    git push --tags; \
	else \
	    echo "already tagged with ${PKGTAG}"; \
	fi

${TARBALL}: ${FILES}
	rm -rf dist
	mkdir -p ${DISTDIR}
	cp -a README* Changes eg Makefile ${PKGNAME}.spec VERSION bin lib ${DISTDIR}
	cd dist; tar cvfz ../${TARBALL} ${FULLPKGNAME}

sources: ${TARBALL}

rpm: ${TARBALL}
	rpmbuild -ta ${TARBALL}

srpm: ${TARBALL}
	rpmbuild -ts ${TARBALL}

clean:
	rm -rf man dist ${TARBALL}
